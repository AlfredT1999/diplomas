﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblEstruorganicaJerarquium
    {
        public int Id { get; set; }
        public int TblEstruOrganicaId { get; set; }
        public int? InmediatoSiguiente { get; set; }
        public int? Subdireccion { get; set; }
    }
}

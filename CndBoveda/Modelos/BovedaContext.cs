﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CndBoveda.Modelos
{
    public partial class bovedaContext : DbContext
    {
        public bovedaContext()
        {
        }

        public bovedaContext(DbContextOptions<bovedaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ArchivosOrganismo> ArchivosOrganismos { get; set; } = null!;
        public virtual DbSet<CatAccionesSistema> CatAccionesSistemas { get; set; } = null!;
        public virtual DbSet<CatAlimento> CatAlimentos { get; set; } = null!;
        public virtual DbSet<CatArchivoOrganismo> CatArchivoOrganismos { get; set; } = null!;
        public virtual DbSet<CatCargoDirectivo> CatCargoDirectivos { get; set; } = null!;
        public virtual DbSet<CatClavealfaEstatus> CatClavealfaEstatuses { get; set; } = null!;
        public virtual DbSet<CatDeporte> CatDeportes { get; set; } = null!;
        public virtual DbSet<CatDisciplina> CatDisciplinas { get; set; } = null!;
        public virtual DbSet<CatDocumentosOrganismo> CatDocumentosOrganismos { get; set; } = null!;
        public virtual DbSet<CatEscolaridad> CatEscolaridads { get; set; } = null!;
        public virtual DbSet<CatEspecialidad> CatEspecialidads { get; set; } = null!;
        public virtual DbSet<CatEstado> CatEstados { get; set; } = null!;
        public virtual DbSet<CatEstadoCivil> CatEstadoCivils { get; set; } = null!;
        public virtual DbSet<CatEstatusDocumento> CatEstatusDocumentos { get; set; } = null!;
        public virtual DbSet<CatEstatusSolicitud> CatEstatusSolicituds { get; set; } = null!;
        public virtual DbSet<CatEstatusestudio> CatEstatusestudios { get; set; } = null!;
        public virtual DbSet<CatEstruorganicaClave> CatEstruorganicaClaves { get; set; } = null!;
        public virtual DbSet<CatGenero> CatGeneros { get; set; } = null!;
        public virtual DbSet<CatInstalacione> CatInstalaciones { get; set; } = null!;
        public virtual DbSet<CatModalidad> CatModalidads { get; set; } = null!;
        public virtual DbSet<CatModuloSistema> CatModuloSistemas { get; set; } = null!;
        public virtual DbSet<CatMunicipio> CatMunicipios { get; set; } = null!;
        public virtual DbSet<CatPaise> CatPaises { get; set; } = null!;
        public virtual DbSet<CatPersonafisica> CatPersonafisicas { get; set; } = null!;
        public virtual DbSet<CatPrograma> CatProgramas { get; set; } = null!;
        public virtual DbSet<CatSede> CatSedes { get; set; } = null!;
        public virtual DbSet<CatSeguridadSocial> CatSeguridadSocials { get; set; } = null!;
        public virtual DbSet<CatSistema> CatSistemas { get; set; } = null!;
        public virtual DbSet<CatSolicitudPersonafisica> CatSolicitudPersonafisicas { get; set; } = null!;
        public virtual DbSet<CatTipoContacto> CatTipoContactos { get; set; } = null!;
        public virtual DbSet<CatTipoInstalacionSede> CatTipoInstalacionSedes { get; set; } = null!;
        public virtual DbSet<CatTipoInstalacione> CatTipoInstalaciones { get; set; } = null!;
        public virtual DbSet<CatTipoOrganismo> CatTipoOrganismos { get; set; } = null!;
        public virtual DbSet<CatTipoPersonafisica> CatTipoPersonafisicas { get; set; } = null!;
        public virtual DbSet<CatTipoRegiman> CatTipoRegimen { get; set; } = null!;
        public virtual DbSet<CatUbicaciongeografica> CatUbicaciongeograficas { get; set; } = null!;
        public virtual DbSet<CatUsuario> CatUsuarios { get; set; } = null!;
        public virtual DbSet<CatUsuarioEstatus> CatUsuarioEstatuses { get; set; } = null!;
        public virtual DbSet<RelDepDiscEsp> RelDepDiscEsps { get; set; } = null!;
        public virtual DbSet<RelDocumentosOrganismo> RelDocumentosOrganismos { get; set; } = null!;
        public virtual DbSet<RelEstadosMunicipio> RelEstadosMunicipios { get; set; } = null!;
        public virtual DbSet<RelOrganismosDeporte> RelOrganismosDeportes { get; set; } = null!;
        public virtual DbSet<RelPersonasFisica> RelPersonasFisicas { get; set; } = null!;
        public virtual DbSet<RelPersonasFisicasSolicitud> RelPersonasFisicasSolicituds { get; set; } = null!;
        public virtual DbSet<RelSedeInstalacione> RelSedeInstalaciones { get; set; } = null!;
        public virtual DbSet<RelUsuarioEstruorganica> RelUsuarioEstruorganicas { get; set; } = null!;
        public virtual DbSet<RelUsuarioOrganismo> RelUsuarioOrganismos { get; set; } = null!;
        public virtual DbSet<RelUsuarioSistema> RelUsuarioSistemas { get; set; } = null!;
        public virtual DbSet<RudsOrganismosRenadeTest> RudsOrganismosRenadeTests { get; set; } = null!;
        public virtual DbSet<TblAfiliacionClub> TblAfiliacionClubs { get; set; } = null!;
        public virtual DbSet<TblAfiliacionClubDirectivo> TblAfiliacionClubDirectivos { get; set; } = null!;
        public virtual DbSet<TblClavealfaOrganismo> TblClavealfaOrganismos { get; set; } = null!;
        public virtual DbSet<TblClavealfaPersonafisica> TblClavealfaPersonafisicas { get; set; } = null!;
        public virtual DbSet<TblCorreo> TblCorreos { get; set; } = null!;
        public virtual DbSet<TblEstatusOrganismo> TblEstatusOrganismos { get; set; } = null!;
        public virtual DbSet<TblEstruorganica> TblEstruorganicas { get; set; } = null!;
        public virtual DbSet<TblEstruorganicaJerarquium> TblEstruorganicaJerarquia { get; set; } = null!;
        public virtual DbSet<TblFederacione> TblFederaciones { get; set; } = null!;
        public virtual DbSet<TblFisicaSolicitud> TblFisicaSolicituds { get; set; } = null!;
        public virtual DbSet<TblFisicaSolicitudDocumento> TblFisicaSolicitudDocumentos { get; set; } = null!;
        public virtual DbSet<TblLogMovUsuario> TblLogMovUsuarios { get; set; } = null!;
        public virtual DbSet<TblLogSistema> TblLogSistemas { get; set; } = null!;
        public virtual DbSet<TblMovUsuario> TblMovUsuarios { get; set; } = null!;
        public virtual DbSet<TblOrganismo> TblOrganismos { get; set; } = null!;
        public virtual DbSet<TblOrganismosDirectivo> TblOrganismosDirectivos { get; set; } = null!;
        public virtual DbSet<TblOrganismosDirectivosperiodo> TblOrganismosDirectivosperiodos { get; set; } = null!;
        public virtual DbSet<TblOrganismosDocumento> TblOrganismosDocumentos { get; set; } = null!;
        public virtual DbSet<TblOrganismosSolicitud> TblOrganismosSolicituds { get; set; } = null!;
        public virtual DbSet<TblPersonafisicaDocumento> TblPersonafisicaDocumentos { get; set; } = null!;
        public virtual DbSet<TblRudFormato> TblRudFormatos { get; set; } = null!;
        public virtual DbSet<TblRudFormatoCoordenada> TblRudFormatoCoordenadas { get; set; } = null!;
        public virtual DbSet<TblRudOrganismo> TblRudOrganismos { get; set; } = null!;
        public virtual DbSet<Tblog> Tblogs { get; set; } = null!;
        public virtual DbSet<ViewCatPersonafisica> ViewCatPersonafisicas { get; set; } = null!;
        public virtual DbSet<ViewCatUsuario> ViewCatUsuarios { get; set; } = null!;
        public virtual DbSet<ViewFederacione> ViewFederaciones { get; set; } = null!;
        public virtual DbSet<ViewTblOrganismo> ViewTblOrganismos { get; set; } = null!;
        public virtual DbSet<VwRandom> VwRandoms { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=10.10.0.32\\MSSQLSERVER2017;user=juanma;password=123;database=boveda");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ArchivosOrganismo>(entity =>
            {
                entity.ToTable("ARCHIVOS_ORGANISMOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Anio)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("anio");

                entity.Property(e => e.CatArchivoOrganismoId).HasColumnName("CAT_ARCHIVO_ORGANISMO_ID");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.TblOrganismoId).HasColumnName("TBL_ORGANISMO_ID");

                entity.Property(e => e.TokenDocumento)
                    .HasMaxLength(150)
                    .IsUnicode(false)
                    .HasColumnName("token_documento");

                entity.HasOne(d => d.CatArchivoOrganismo)
                    .WithMany(p => p.ArchivosOrganismos)
                    .HasForeignKey(d => d.CatArchivoOrganismoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ARCHIVOS___CAT_A__67FE6514");
            });

            modelBuilder.Entity<CatAccionesSistema>(entity =>
            {
                entity.ToTable("CAT_ACCIONES_SISTEMA");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Accion).HasMaxLength(300);
            });

            modelBuilder.Entity<CatAlimento>(entity =>
            {
                entity.ToTable("CAT_ALIMENTOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasColumnName("ACTIVO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Alimento)
                    .HasMaxLength(200)
                    .HasColumnName("ALIMENTO");

                entity.Property(e => e.Fin).HasColumnName("fin");

                entity.Property(e => e.Inicio).HasColumnName("inicio");
            });

            modelBuilder.Entity<CatArchivoOrganismo>(entity =>
            {
                entity.ToTable("CAT_ARCHIVO_ORGANISMO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasColumnName("activo")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CatTipoOrganismoId).HasColumnName("CAT_TIPO_ORGANISMO_ID");

                entity.Property(e => e.DocumentoBase).HasColumnName("documento_base");

                entity.Property(e => e.NombreDocumento)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("nombre_documento");
            });

            modelBuilder.Entity<CatCargoDirectivo>(entity =>
            {
                entity.ToTable("CAT_CARGO_DIRECTIVO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Cargo)
                    .HasMaxLength(300)
                    .HasColumnName("cargo");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");
            });

            modelBuilder.Entity<CatClavealfaEstatus>(entity =>
            {
                entity.ToTable("CAT_CLAVEALFA_ESTATUS");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .HasColumnName("DESCRIPCION");

                entity.Property(e => e.Estatus)
                    .HasMaxLength(500)
                    .HasColumnName("ESTATUS");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("INCLUSION")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<CatDeporte>(entity =>
            {
                entity.ToTable("CAT_DEPORTE");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Deporte).HasMaxLength(400);

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<CatDisciplina>(entity =>
            {
                entity.ToTable("CAT_DISCIPLINA");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CatDeporteId).HasColumnName("CAT_DEPORTE_ID");

                entity.Property(e => e.Disciplina).HasMaxLength(400);

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.CatDeporte)
                    .WithMany(p => p.CatDisciplinas)
                    .HasForeignKey(d => d.CatDeporteId)
                    .HasConstraintName("FK_CAT_DEPORTE_ID");
            });

            modelBuilder.Entity<CatDocumentosOrganismo>(entity =>
            {
                entity.ToTable("CAT_DOCUMENTOS_ORGANISMOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(250)
                    .HasColumnName("nombre");

                entity.Property(e => e.Siglas)
                    .HasMaxLength(50)
                    .HasColumnName("siglas");
            });

            modelBuilder.Entity<CatEscolaridad>(entity =>
            {
                entity.HasKey(e => e.CatId)
                    .HasName("PK__CAT_ESCO__DD5DDDBD4734A374");

                entity.ToTable("CAT_ESCOLARIDAD");

                entity.Property(e => e.CatId).HasColumnName("cat_id");

                entity.Property(e => e.CatName)
                    .HasMaxLength(300)
                    .HasColumnName("cat_name");
            });

            modelBuilder.Entity<CatEspecialidad>(entity =>
            {
                entity.ToTable("CAT_ESPECIALIDAD");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Especialidad).HasMaxLength(400);

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<CatEstado>(entity =>
            {
                entity.HasKey(e => e.IdEstado)
                    .HasName("PK__CAT_ESTA__62EA894A3F62E364");

                entity.ToTable("CAT_ESTADOS");

                entity.Property(e => e.IdEstado).HasColumnName("idEstado");

                entity.Property(e => e.Estado)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CatEstadoCivil>(entity =>
            {
                entity.ToTable("CAT_ESTADO_CIVIL");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion).HasMaxLength(50);

                entity.Property(e => e.Inclusion).HasColumnType("datetime");
            });

            modelBuilder.Entity<CatEstatusDocumento>(entity =>
            {
                entity.ToTable("CAT_ESTATUS_DOCUMENTOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");
            });

            modelBuilder.Entity<CatEstatusSolicitud>(entity =>
            {
                entity.ToTable("CAT_ESTATUS_SOLICITUD");

                entity.Property(e => e.Descripcion).HasMaxLength(400);

                entity.Property(e => e.Inclusion).HasColumnType("datetime");
            });

            modelBuilder.Entity<CatEstatusestudio>(entity =>
            {
                entity.HasKey(e => e.CatId)
                    .HasName("PK__CAT_ESTA__DD5DDDBDDEC0AFD9");

                entity.ToTable("CAT_ESTATUSESTUDIOS");

                entity.Property(e => e.CatId).HasColumnName("cat_id");

                entity.Property(e => e.CatName)
                    .HasMaxLength(300)
                    .HasColumnName("cat_name");
            });

            modelBuilder.Entity<CatEstruorganicaClave>(entity =>
            {
                entity.ToTable("CAT_ESTRUORGANICA_CLAVE");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion).HasMaxLength(100);

                entity.Property(e => e.Sigla).HasMaxLength(10);
            });

            modelBuilder.Entity<CatGenero>(entity =>
            {
                entity.HasKey(e => e.CatId)
                    .HasName("PK__CAT_GENE__DD5DDDBD80C09226");

                entity.ToTable("CAT_GENERO");

                entity.Property(e => e.CatId).HasColumnName("cat_id");

                entity.Property(e => e.CatName)
                    .HasMaxLength(300)
                    .HasColumnName("cat_name");
            });

            modelBuilder.Entity<CatInstalacione>(entity =>
            {
                entity.HasKey(e => e.IdInstalacion)
                    .HasName("PK__CAT_INST__CC1E762094B26DF7");

                entity.ToTable("CAT_INSTALACIONES");

                entity.Property(e => e.IdInstalacion).HasColumnName("idInstalacion");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CatModalidad>(entity =>
            {
                entity.ToTable("CAT_MODALIDAD");

                entity.Property(e => e.Descripcion).HasMaxLength(200);

                entity.Property(e => e.Incluision).HasColumnType("datetime");
            });

            modelBuilder.Entity<CatModuloSistema>(entity =>
            {
                entity.ToTable("CAT_MODULO_SISTEMA");

                entity.HasIndex(e => new { e.CatSistemaId, e.Modulo }, "UQ_Module")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatSistemaId).HasColumnName("CAT_SISTEMA_ID");

                entity.Property(e => e.Modulo).HasMaxLength(300);

                entity.HasOne(d => d.CatSistema)
                    .WithMany(p => p.CatModuloSistemas)
                    .HasForeignKey(d => d.CatSistemaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CAT_MODULO_SISTEMA_CAT_SISTEMA");
            });

            modelBuilder.Entity<CatMunicipio>(entity =>
            {
                entity.HasKey(e => e.IdMunicipio)
                    .HasName("PK__CAT_MUNI__FD10E400F65D5A8C");

                entity.ToTable("CAT_MUNICIPIOS");

                entity.Property(e => e.IdMunicipio).HasColumnName("idMunicipio");

                entity.Property(e => e.Municipio)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CatPaise>(entity =>
            {
                entity.ToTable("CAT_PAISES");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Iso)
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasColumnName("iso")
                    .IsFixedLength();

                entity.Property(e => e.Nombre)
                    .HasMaxLength(80)
                    .IsUnicode(false)
                    .HasColumnName("nombre");
            });

            modelBuilder.Entity<CatPersonafisica>(entity =>
            {
                entity.HasKey(e => e.IdPersona)
                    .HasName("PK__CAT_PERS__A4788141289B3572");

                entity.ToTable("CAT_PERSONAFISICA");

                entity.Property(e => e.IdPersona).HasColumnName("idPersona");

                entity.Property(e => e.Calle)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CatUbicaciongeograficaId).HasColumnName("CAT_UBICACIONGEOGRAFICA_ID");

                entity.Property(e => e.Colonia)
                    .HasMaxLength(150)
                    .HasColumnName("colonia");

                entity.Property(e => e.ComentarioInactivo)
                    .HasMaxLength(500)
                    .HasColumnName("comentarioInactivo");

                entity.Property(e => e.Curp)
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .HasColumnName("CURP");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.FechaNacimiento).HasColumnType("date");

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.Fm3)
                    .HasMaxLength(400)
                    .HasColumnName("fm3");

                entity.Property(e => e.IdEscolaridad).HasColumnName("idEscolaridad");

                entity.Property(e => e.IdEstatusEscolaridad).HasColumnName("idEstatusEscolaridad");

                entity.Property(e => e.IdGenero).HasColumnName("idGenero");

                entity.Property(e => e.Materno)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NoPasaporte)
                    .HasMaxLength(400)
                    .HasColumnName("noPasaporte");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NumExt)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("Num_Ext");

                entity.Property(e => e.NumInt)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("Num_Int");

                entity.Property(e => e.NumeroIdentificacion).HasMaxLength(300);

                entity.Property(e => e.Paterno)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TblEquipoMultidiciplinarioId).HasColumnName("tbl_equipo_multidiciplinario_id");

                entity.Property(e => e.TblOrganismosId).HasColumnName("TBL_ORGANISMOS_ID");

                entity.Property(e => e.TblPaisId).HasColumnName("tbl_pais_id");

                entity.Property(e => e.TipoIdentificacion).HasMaxLength(300);

                entity.Property(e => e.TokenFoto)
                    .HasMaxLength(600)
                    .HasColumnName("token_foto");

                entity.HasOne(d => d.CatUbicaciongeografica)
                    .WithMany(p => p.CatPersonafisicas)
                    .HasForeignKey(d => d.CatUbicaciongeograficaId)
                    .HasConstraintName("FK__CAT_PERSO__CAT_U__7AF13DF7");
            });

            modelBuilder.Entity<CatPrograma>(entity =>
            {
                entity.ToTable("CAT_PROGRAMA");

                entity.Property(e => e.Inclusion).HasColumnType("datetime");

                entity.Property(e => e.Programa).HasMaxLength(400);
            });

            modelBuilder.Entity<CatSede>(entity =>
            {
                entity.HasKey(e => e.IdSede)
                    .HasName("PK__CAT_SEDE__C5AF63D0C67A4219");

                entity.ToTable("CAT_SEDE");

                entity.Property(e => e.IdSede).HasColumnName("idSede");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CatSeguridadSocial>(entity =>
            {
                entity.ToTable("CAT_SEGURIDAD_SOCIAL");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatSeguridadSocial1)
                    .HasMaxLength(100)
                    .HasColumnName("CAT_SEGURIDAD_SOCIAL");
            });

            modelBuilder.Entity<CatSistema>(entity =>
            {
                entity.HasKey(e => e.SId);

                entity.ToTable("CAT_SISTEMA");

                entity.Property(e => e.SId)
                    .ValueGeneratedNever()
                    .HasColumnName("s_id");

                entity.Property(e => e.SDescripcion).HasColumnName("s_descripcion");

                entity.Property(e => e.SSiatema)
                    .HasMaxLength(500)
                    .HasColumnName("s_siatema");
            });

            modelBuilder.Entity<CatSolicitudPersonafisica>(entity =>
            {
                entity.HasKey(e => e.IdPersona)
                    .HasName("PK__CAT_SOLI__A478814190D047C8");

                entity.ToTable("CAT_SOLICITUD_PERSONAFISICA");

                entity.Property(e => e.IdPersona).HasColumnName("idPersona");

                entity.Property(e => e.Calle)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CatTipoPersonafisicaId).HasColumnName("CAT_TIPO_PERSONAFISICA_ID");

                entity.Property(e => e.CatUbicaciongeograficaId).HasColumnName("CAT_UBICACIONGEOGRAFICA_ID");

                entity.Property(e => e.Colonia)
                    .HasMaxLength(150)
                    .HasColumnName("colonia");

                entity.Property(e => e.ComentarioInactivo)
                    .HasMaxLength(500)
                    .HasColumnName("comentarioInactivo");

                entity.Property(e => e.Comentarios).HasMaxLength(600);

                entity.Property(e => e.Curp)
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .HasColumnName("CURP");

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.FechaNacimiento).HasColumnType("date");

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.Fm3)
                    .HasMaxLength(400)
                    .HasColumnName("fm3");

                entity.Property(e => e.IdEscolaridad).HasColumnName("idEscolaridad");

                entity.Property(e => e.IdEstatusEscolaridad).HasColumnName("idEstatusEscolaridad");

                entity.Property(e => e.IdGenero).HasColumnName("idGenero");

                entity.Property(e => e.Materno)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NoPasaporte)
                    .HasMaxLength(400)
                    .HasColumnName("noPasaporte");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NumExt)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("Num_Ext");

                entity.Property(e => e.NumInt)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("Num_Int");

                entity.Property(e => e.NumeroIdentificacion).HasMaxLength(300);

                entity.Property(e => e.Paterno)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TblEquipoMultidiciplinarioId).HasColumnName("tbl_equipo_multidiciplinario_id");

                entity.Property(e => e.TblOrganismosId).HasColumnName("TBL_ORGANISMOS_ID");

                entity.Property(e => e.TblPaisId).HasColumnName("tbl_pais_id");

                entity.Property(e => e.TipoIdentificacion).HasMaxLength(300);

                entity.Property(e => e.TokenFoto)
                    .HasMaxLength(600)
                    .HasColumnName("token_foto");
            });

            modelBuilder.Entity<CatTipoContacto>(entity =>
            {
                entity.ToTable("CAT_TIPO_CONTACTO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Tipo)
                    .HasMaxLength(100)
                    .HasColumnName("TIPO");

                entity.Property(e => e.TipoContacto)
                    .HasMaxLength(100)
                    .HasColumnName("TIPO_CONTACTO");
            });

            modelBuilder.Entity<CatTipoInstalacionSede>(entity =>
            {
                entity.ToTable("CAT_TIPO_INSTALACION_SEDE");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasColumnName("ACTIVO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CatSedeId).HasColumnName("CAT_SEDE_ID");

                entity.Property(e => e.CatTipoInstalacionesId).HasColumnName("CAT_TIPO_INSTALACIONES_ID");

                entity.HasOne(d => d.CatSede)
                    .WithMany(p => p.CatTipoInstalacionSedes)
                    .HasForeignKey(d => d.CatSedeId)
                    .HasConstraintName("FK__CAT_TIPO___CAT_S__3FF073BA");

                entity.HasOne(d => d.CatTipoInstalaciones)
                    .WithMany(p => p.CatTipoInstalacionSedes)
                    .HasForeignKey(d => d.CatTipoInstalacionesId)
                    .HasConstraintName("FK__CAT_TIPO___CAT_T__3EFC4F81");
            });

            modelBuilder.Entity<CatTipoInstalacione>(entity =>
            {
                entity.ToTable("CAT_TIPO_INSTALACIONES");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo)
                    .IsRequired()
                    .HasColumnName("ACTIVO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Instalacion)
                    .HasMaxLength(200)
                    .HasColumnName("INSTALACION");
            });

            modelBuilder.Entity<CatTipoOrganismo>(entity =>
            {
                entity.ToTable("CAT_TIPO_ORGANISMOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Renade).HasColumnName("RENADE");

                entity.Property(e => e.Sinade).HasColumnName("SINADE");
            });

            modelBuilder.Entity<CatTipoPersonafisica>(entity =>
            {
                entity.HasKey(e => e.IdTipoPersonaFisica)
                    .HasName("PK__CAT_TIPO__0EF3899B806C1CC4");

                entity.ToTable("CAT_TIPO_PERSONAFISICA");

                entity.Property(e => e.IdTipoPersonaFisica).HasColumnName("idTipoPersonaFisica");

                entity.Property(e => e.EsPersonalTecnico)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Inclusión).HasColumnType("datetime");

                entity.Property(e => e.TipoPersonaFisica)
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CatTipoRegiman>(entity =>
            {
                entity.HasKey(e => e.TcrId)
                    .HasName("PK__CAT_TIPO__900708C563CF8DB2");

                entity.ToTable("CAT_TIPO_REGIMEN");

                entity.Property(e => e.TcrId).HasColumnName("tcr_id");

                entity.Property(e => e.TcrTipo)
                    .HasMaxLength(500)
                    .HasColumnName("tcr_tipo");
            });

            modelBuilder.Entity<CatUbicaciongeografica>(entity =>
            {
                entity.ToTable("CAT_UBICACIONGEOGRAFICA");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatEstadoId).HasColumnName("CAT_ESTADO_ID");

                entity.Property(e => e.Ciudad).HasMaxLength(500);

                entity.Property(e => e.CodigoPostal).HasMaxLength(5);

                entity.Property(e => e.Colonia).HasMaxLength(500);

                entity.Property(e => e.Estado).HasMaxLength(500);

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.Municipio).HasMaxLength(500);

                entity.HasOne(d => d.CatEstado)
                    .WithMany(p => p.CatUbicaciongeograficas)
                    .HasForeignKey(d => d.CatEstadoId)
                    .HasConstraintName("FK__CAT_UBICA__CAT_E__1B9317B3");
            });

            modelBuilder.Entity<CatUsuario>(entity =>
            {
                entity.HasKey(e => e.UsId)
                    .HasName("PK__CAT_USUA__2E701A673DBB588C");

                entity.ToTable("CAT_USUARIO");

                entity.HasIndex(e => e.UsCorreo, "CAT_USUARIO_CORREO")
                    .IsUnique();

                entity.HasIndex(e => e.UsCuenta, "CAT_USUARIO_CUENTA")
                    .IsUnique();

                entity.HasIndex(e => e.UsExpediente, "CAT_USUARIO_EXPEDIENTE")
                    .IsUnique();

                entity.Property(e => e.UsId).HasColumnName("us_id");

                entity.Property(e => e.EnvioToken)
                    .HasColumnType("datetime")
                    .HasColumnName("envio_token");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TcrId).HasColumnName("tcr_id");

                entity.Property(e => e.UsCorreo)
                    .HasMaxLength(500)
                    .HasColumnName("us_correo");

                entity.Property(e => e.UsCuenta)
                    .HasMaxLength(500)
                    .HasColumnName("us_cuenta");

                entity.Property(e => e.UsExpediente).HasColumnName("us_expediente");

                entity.Property(e => e.UsIntentos).HasColumnName("us_intentos");

                entity.Property(e => e.UsMaterno)
                    .HasMaxLength(500)
                    .HasColumnName("us_materno");

                entity.Property(e => e.UsNombres)
                    .HasMaxLength(500)
                    .HasColumnName("us_nombres");

                entity.Property(e => e.UsPassword)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("us_password");

                entity.Property(e => e.UsPaterno)
                    .HasMaxLength(500)
                    .HasColumnName("us_paterno");

                entity.Property(e => e.UsPuesto)
                    .HasMaxLength(500)
                    .HasColumnName("us_puesto");

                entity.Property(e => e.UsesId).HasColumnName("uses_id");

                entity.HasOne(d => d.Tcr)
                    .WithMany(p => p.CatUsuarios)
                    .HasForeignKey(d => d.TcrId)
                    .HasConstraintName("FK__CAT_USUAR__tcr_i__440B1D61");

                entity.HasOne(d => d.Uses)
                    .WithMany(p => p.CatUsuarios)
                    .HasForeignKey(d => d.UsesId)
                    .HasConstraintName("FK__CAT_USUAR__uses___44FF419A");
            });

            modelBuilder.Entity<CatUsuarioEstatus>(entity =>
            {
                entity.HasKey(e => e.UsesId)
                    .HasName("PK__CAT_USUA__347AF994A96B331B");

                entity.ToTable("CAT_USUARIO_ESTATUS");

                entity.Property(e => e.UsesId).HasColumnName("USES_id");

                entity.Property(e => e.UsesDescripcio)
                    .HasMaxLength(2000)
                    .HasColumnName("USES_descripcio");

                entity.Property(e => e.UsesEstatus)
                    .HasMaxLength(500)
                    .HasColumnName("USES_estatus");
            });

            modelBuilder.Entity<RelDepDiscEsp>(entity =>
            {
                entity.ToTable("REL_DEP_DISC_ESP");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatDeporteId).HasColumnName("CAT_DEPORTE_ID");

                entity.Property(e => e.CatDiciplinaId).HasColumnName("CAT_DICIPLINA_ID");

                entity.Property(e => e.CatEspecialidadId).HasColumnName("CAT_ESPECIALIDAD_ID");

                entity.HasOne(d => d.CatDeporte)
                    .WithMany(p => p.RelDepDiscEsps)
                    .HasForeignKey(d => d.CatDeporteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_DEP_D__CAT_D__7FB5F314");

                entity.HasOne(d => d.CatDiciplina)
                    .WithMany(p => p.RelDepDiscEsps)
                    .HasForeignKey(d => d.CatDiciplinaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_DEP_D__CAT_D__00AA174D");

                entity.HasOne(d => d.CatEspecialidad)
                    .WithMany(p => p.RelDepDiscEsps)
                    .HasForeignKey(d => d.CatEspecialidadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_DEP_D__CAT_E__019E3B86");
            });

            modelBuilder.Entity<RelDocumentosOrganismo>(entity =>
            {
                entity.ToTable("REL_DOCUMENTOS_ORGANISMOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdDocumentoOrganismo).HasColumnName("idDocumentoOrganismo");

                entity.Property(e => e.IdTipoOrganismo).HasColumnName("idTipoOrganismo");
            });

            modelBuilder.Entity<RelEstadosMunicipio>(entity =>
            {
                entity.ToTable("REL_ESTADOS_MUNICIPIOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdEstado).HasColumnName("idEstado");

                entity.Property(e => e.IdMunicipio).HasColumnName("idMunicipio");

                entity.HasOne(d => d.IdEstadoNavigation)
                    .WithMany(p => p.RelEstadosMunicipios)
                    .HasForeignKey(d => d.IdEstado)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_ESTAD__idEst__57DD0BE4");

                entity.HasOne(d => d.IdMunicipioNavigation)
                    .WithMany(p => p.RelEstadosMunicipios)
                    .HasForeignKey(d => d.IdMunicipio)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_ESTAD__idMun__58D1301D");
            });

            modelBuilder.Entity<RelOrganismosDeporte>(entity =>
            {
                entity.ToTable("REL_ORGANISMOS_DEPORTE");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatDeporteId).HasColumnName("CAT_DEPORTE_ID");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TblOrganismosId).HasColumnName("TBL_ORGANISMOS_ID");

                entity.HasOne(d => d.CatDeporte)
                    .WithMany(p => p.RelOrganismosDeportes)
                    .HasForeignKey(d => d.CatDeporteId)
                    .HasConstraintName("FK__REL_ORGAN__CAT_D__02925FBF");

                entity.HasOne(d => d.TblOrganismos)
                    .WithMany(p => p.RelOrganismosDeportes)
                    .HasForeignKey(d => d.TblOrganismosId)
                    .HasConstraintName("FK__REL_ORGAN__TBL_O__038683F8");
            });

            modelBuilder.Entity<RelPersonasFisica>(entity =>
            {
                entity.ToTable("REL_PERSONAS_FISICAS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatPersonafisicaId).HasColumnName("CAT_PERSONAFISICA_ID");

                entity.Property(e => e.CatTipoPersonafisicaId).HasColumnName("CAT_TIPO_PERSONAFISICA_ID");

                entity.Property(e => e.RelDepDiscEspId).HasColumnName("REL_DEP_DISC_ESP_ID");

                entity.HasOne(d => d.CatPersonafisica)
                    .WithMany(p => p.RelPersonasFisicas)
                    .HasForeignKey(d => d.CatPersonafisicaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_PERSO__CAT_P__0B27A5C0");

                entity.HasOne(d => d.CatTipoPersonafisica)
                    .WithMany(p => p.RelPersonasFisicas)
                    .HasForeignKey(d => d.CatTipoPersonafisicaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_PERSO__CAT_T__57A801BA21");

                entity.HasOne(d => d.RelDepDiscEsp)
                    .WithMany(p => p.RelPersonasFisicas)
                    .HasForeignKey(d => d.RelDepDiscEspId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_PERSO__REL_D__0FEC5ADD");
            });

            modelBuilder.Entity<RelPersonasFisicasSolicitud>(entity =>
            {
                entity.ToTable("REL_PERSONAS_FISICAS_SOLICITUD");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatPersonafisicaId).HasColumnName("CAT_PERSONAFISICA_ID");

                entity.Property(e => e.CatTipoPersonafisicaId).HasColumnName("CAT_TIPO_PERSONAFISICA_ID");

                entity.Property(e => e.Estatus)
                    .HasMaxLength(60)
                    .HasColumnName("ESTATUS");

                entity.Property(e => e.Justificacion).HasColumnName("JUSTIFICACION");

                entity.Property(e => e.RelDepDiscEspId).HasColumnName("REL_DEP_DISC_ESP_ID");
            });

            modelBuilder.Entity<RelSedeInstalacione>(entity =>
            {
                entity.HasKey(e => e.IdSedeInstalacion)
                    .HasName("PK__REL_SEDE__9592627D2EDA76B0");

                entity.ToTable("REL_SEDE_INSTALACIONES");

                entity.Property(e => e.IdSedeInstalacion).HasColumnName("id_Sede_Instalacion");

                entity.Property(e => e.IdInstalacion).HasColumnName("idInstalacion");

                entity.Property(e => e.IdSede).HasColumnName("idSede");

                entity.HasOne(d => d.IdInstalacionNavigation)
                    .WithMany(p => p.RelSedeInstalaciones)
                    .HasForeignKey(d => d.IdInstalacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_idInstalacion");

                entity.HasOne(d => d.IdSedeNavigation)
                    .WithMany(p => p.RelSedeInstalaciones)
                    .HasForeignKey(d => d.IdSede)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_idSede");
            });

            modelBuilder.Entity<RelUsuarioEstruorganica>(entity =>
            {
                entity.HasKey(e => e.UsuarioEstruorganicaId)
                    .HasName("PK__REL_USUA__6E25DDBFFF4CA06E");

                entity.ToTable("REL_USUARIO_ESTRUORGANICA");

                entity.Property(e => e.UsuarioEstruorganicaId).HasColumnName("usuario_estruorganica_id");

                entity.Property(e => e.CatUsuarioId).HasColumnName("cat_usuario_id");

                entity.Property(e => e.EstruorganicaId).HasColumnName("estruorganica_id");

                entity.HasOne(d => d.CatUsuario)
                    .WithMany(p => p.RelUsuarioEstruorganicas)
                    .HasForeignKey(d => d.CatUsuarioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_USUAR__cat_u__5DCAEF64");

                entity.HasOne(d => d.Estruorganica)
                    .WithMany(p => p.RelUsuarioEstruorganicas)
                    .HasForeignKey(d => d.EstruorganicaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__REL_USUAR__estru__5EBF139D");
            });

            modelBuilder.Entity<RelUsuarioOrganismo>(entity =>
            {
                entity.ToTable("REL_USUARIO_ORGANISMO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatUsuarioId).HasColumnName("cat_usuario_id");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TblOrganismoId).HasColumnName("tbl_organismo_id");
            });

            modelBuilder.Entity<RelUsuarioSistema>(entity =>
            {
                entity.HasKey(e => e.SusId);

                entity.ToTable("REL_USUARIO_SISTEMA");

                entity.Property(e => e.SusId).HasColumnName("sus_id");

                entity.Property(e => e.SusActivo).HasColumnName("sus_activo");

                entity.Property(e => e.SusIdSistema).HasColumnName("sus_id_sistema");

                entity.Property(e => e.SusIdUsuario).HasColumnName("sus_id_usuario");

                entity.HasOne(d => d.SusIdSistemaNavigation)
                    .WithMany(p => p.RelUsuarioSistemas)
                    .HasForeignKey(d => d.SusIdSistema)
                    .HasConstraintName("FK_REL_USUARIO_SISTEMA_CAT_SISTEMA");

                entity.HasOne(d => d.SusIdUsuarioNavigation)
                    .WithMany(p => p.RelUsuarioSistemas)
                    .HasForeignKey(d => d.SusIdUsuario)
                    .HasConstraintName("FK_REL_USUARIO_SISTEMA_CAT_USUARIO");
            });

            modelBuilder.Entity<RudsOrganismosRenadeTest>(entity =>
            {
                entity.ToTable("RUDS_ORGANISMOS_RENADE_TEST");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.RazonSocialRenade)
                    .IsUnicode(false)
                    .HasColumnName("razon_social_renade");

                entity.Property(e => e.RfcRenade)
                    .IsUnicode(false)
                    .HasColumnName("rfc_renade");

                entity.Property(e => e.RudIdRenade).HasColumnName("rud_id_renade");

                entity.Property(e => e.RudRenade)
                    .IsUnicode(false)
                    .HasColumnName("rud_renade");

                entity.Property(e => e.SolExpiryDateRenade)
                    .HasColumnType("datetime")
                    .HasColumnName("sol_expiry_date_renade");

                entity.Property(e => e.SolIdRenade).HasColumnName("sol_id_renade");
            });

            modelBuilder.Entity<TblAfiliacionClub>(entity =>
            {
                entity.ToTable("TBL_AFILIACION_CLUB");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Aprobado).HasColumnName("aprobado");

                entity.Property(e => e.CatDeporteId).HasColumnName("CAT_DEPORTE_ID");

                entity.Property(e => e.CatDisciplinaId).HasColumnName("CAT_DISCIPLINA_ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Direccion).HasColumnName("direccion");

                entity.Property(e => e.Fecha)
                    .HasColumnType("date")
                    .HasColumnName("fecha");

                entity.Property(e => e.FechaFin)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaFin");

                entity.Property(e => e.FechaInicio)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaInicio");

                entity.Property(e => e.Hora)
                    .HasMaxLength(50)
                    .HasColumnName("hora");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.Organismo)
                    .HasMaxLength(500)
                    .HasColumnName("organismo");

                entity.Property(e => e.TblOrganismosId).HasColumnName("TBL_ORGANISMOS_ID");

                entity.HasOne(d => d.CatDeporte)
                    .WithMany(p => p.TblAfiliacionClubs)
                    .HasForeignKey(d => d.CatDeporteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TBL_AFILIACION_CLUB_CAT_DEPORTE");

                entity.HasOne(d => d.CatDisciplina)
                    .WithMany(p => p.TblAfiliacionClubs)
                    .HasForeignKey(d => d.CatDisciplinaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TBL_AFILIACION_CLUB_CAT_DISCIPLINA");

                entity.HasOne(d => d.TblOrganismos)
                    .WithMany(p => p.TblAfiliacionClubs)
                    .HasForeignKey(d => d.TblOrganismosId)
                    .HasConstraintName("FK_TBL_AFILIACION_CLUB_TBL_ORGANISMOS");
            });

            modelBuilder.Entity<TblAfiliacionClubDirectivo>(entity =>
            {
                entity.ToTable("TBL_AFILIACION_CLUB_DIRECTIVOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatCargoDirectivoId).HasColumnName("CAT_CARGO_DIRECTIVO_ID");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(500)
                    .HasColumnName("nombre");

                entity.Property(e => e.TblAfiliacionClubId).HasColumnName("TBL_AFILIACION_CLUB_ID");

                entity.HasOne(d => d.CatCargoDirectivo)
                    .WithMany(p => p.TblAfiliacionClubDirectivos)
                    .HasForeignKey(d => d.CatCargoDirectivoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TBL_AFILIACION_CLUB_DIRECTIVOS_CAT_CARGO_DIRECTIVO");

                entity.HasOne(d => d.TblAfiliacionClub)
                    .WithMany(p => p.TblAfiliacionClubDirectivos)
                    .HasForeignKey(d => d.TblAfiliacionClubId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TBL_AFILIACION_CLUB_DIRECTIVOS_TBL_AFILIACION_CLUB");
            });

            modelBuilder.Entity<TblClavealfaOrganismo>(entity =>
            {
                entity.ToTable("TBL_CLAVEALFA_ORGANISMOS");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CatClavealfaEstatusId).HasColumnName("CAT_CLAVEALFA_ESTATUS_ID");

                entity.Property(e => e.Expedicion)
                    .HasColumnType("datetime")
                    .HasColumnName("EXPEDICION");

                entity.Property(e => e.Expira)
                    .HasColumnType("datetime")
                    .HasColumnName("EXPIRA");

                entity.Property(e => e.Folio).HasMaxLength(300);

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("INCLUSION")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Inscripcion)
                    .HasColumnType("datetime")
                    .HasColumnName("INSCRIPCION");

                entity.Property(e => e.Modificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("MODIFICACION");

                entity.Property(e => e.Rud)
                    .HasMaxLength(500)
                    .HasColumnName("RUD");

                entity.Property(e => e.TblOrganismosId).HasColumnName("TBL_ORGANISMOS_ID");

                entity.HasOne(d => d.CatClavealfaEstatus)
                    .WithMany(p => p.TblClavealfaOrganismos)
                    .HasForeignKey(d => d.CatClavealfaEstatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_CLAVE__CAT_C__1975C517");

                entity.HasOne(d => d.TblOrganismos)
                    .WithMany(p => p.TblClavealfaOrganismos)
                    .HasForeignKey(d => d.TblOrganismosId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_CLAVE__TBL_O__1A69E950");
            });

            modelBuilder.Entity<TblClavealfaPersonafisica>(entity =>
            {
                entity.ToTable("TBL_CLAVEALFA_PERSONAFISICA");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Alfa)
                    .HasMaxLength(500)
                    .HasColumnName("ALFA");

                entity.Property(e => e.CatClavealfaEstatusId).HasColumnName("CAT_CLAVEALFA_ESTATUS_ID");

                entity.Property(e => e.CatPersonafisicaId).HasColumnName("CAT_PERSONAFISICA_ID");

                entity.Property(e => e.Expedicion)
                    .HasColumnType("datetime")
                    .HasColumnName("EXPEDICION");

                entity.Property(e => e.Expira)
                    .HasColumnType("datetime")
                    .HasColumnName("EXPIRA");

                entity.Property(e => e.Folio)
                    .HasMaxLength(300)
                    .HasColumnName("FOLIO");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("INCLUSION")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Inscripcion)
                    .HasColumnType("datetime")
                    .HasColumnName("INSCRIPCION");

                entity.Property(e => e.Modificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("MODIFICACION");

                entity.HasOne(d => d.CatClavealfaEstatus)
                    .WithMany(p => p.TblClavealfaPersonafisicas)
                    .HasForeignKey(d => d.CatClavealfaEstatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_CLAVE__CAT_C__1B5E0D89");

                entity.HasOne(d => d.CatPersonafisica)
                    .WithMany(p => p.TblClavealfaPersonafisicas)
                    .HasForeignKey(d => d.CatPersonafisicaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_CLAVE__CAT_P__1C5231C2");
            });

            modelBuilder.Entity<TblCorreo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TBL_CORREOS");

                entity.Property(e => e.Correo)
                    .HasMaxLength(600)
                    .HasColumnName("CORREO");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("ID");

                entity.Property(e => e.Organismo)
                    .HasMaxLength(800)
                    .HasColumnName("ORGANISMO");
            });

            modelBuilder.Entity<TblEstatusOrganismo>(entity =>
            {
                entity.ToTable("TBL_ESTATUS_ORGANISMO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estatus).HasMaxLength(400);

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");
            });

            modelBuilder.Entity<TblEstruorganica>(entity =>
            {
                entity.ToTable("TBL_ESTRUORGANICA");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatEstruOrganicaClaveId).HasColumnName("cat_EstruOrganica_Clave_id");

                entity.Property(e => e.Nombre).HasMaxLength(100);

                entity.HasOne(d => d.CatEstruOrganicaClave)
                    .WithMany(p => p.TblEstruorganicas)
                    .HasForeignKey(d => d.CatEstruOrganicaClaveId)
                    .HasConstraintName("FK__tbl_Estru__cat_E__2A4B4B5E");
            });

            modelBuilder.Entity<TblEstruorganicaJerarquium>(entity =>
            {
                entity.ToTable("TBL_ESTRUORGANICA_JERARQUIA");

                entity.HasIndex(e => new { e.TblEstruOrganicaId, e.InmediatoSiguiente }, "tbl_EstruOrganica_Jerarquia_key")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.InmediatoSiguiente).HasColumnName("inmediato_siguiente");

                entity.Property(e => e.Subdireccion).HasColumnName("subdireccion");

                entity.Property(e => e.TblEstruOrganicaId).HasColumnName("tbl_EstruOrganica_id");
            });

            modelBuilder.Entity<TblFederacione>(entity =>
            {
                entity.ToTable("TBL_FEDERACIONES");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IdFederacion).HasColumnName("id_federacion");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");
            });

            modelBuilder.Entity<TblFisicaSolicitud>(entity =>
            {
                entity.ToTable("TBL_FISICA_SOLICITUD");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Calle)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CatDisciplinaId).HasColumnName("CAT_DISCIPLINA_ID");

                entity.Property(e => e.CatEspecialidadId).HasColumnName("CAT_ESPECIALIDAD_ID");

                entity.Property(e => e.CatEstatusSolicitudId).HasColumnName("CAT_ESTATUS_SOLICITUD_ID");

                entity.Property(e => e.CatTipoPersonafisicaId).HasColumnName("CAT_TIPO_PERSONAFISICA_ID");

                entity.Property(e => e.CatUbicaciongeograficaId).HasColumnName("CAT_UBICACIONGEOGRAFICA_ID");

                entity.Property(e => e.CatUsuarioModificaId).HasColumnName("CAT_USUARIO_MODIFICA_ID");

                entity.Property(e => e.Curp)
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .HasColumnName("CURP");

                entity.Property(e => e.FechaNacimiento).HasColumnType("date");

                entity.Property(e => e.Inclusion).HasColumnType("datetime");

                entity.Property(e => e.Materno)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Modifica).HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NumExterior)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NumInterior)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroIdentificacion).HasMaxLength(300);

                entity.Property(e => e.Paterno)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TblOrganismoId).HasColumnName("TBL_ORGANISMO_ID");

                entity.Property(e => e.TipoIdentificacion).HasMaxLength(300);

                entity.Property(e => e.TokenFoto).HasMaxLength(600);

                entity.HasOne(d => d.CatDisciplina)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.CatDisciplinaId)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_DISCIPLINA");

                entity.HasOne(d => d.CatEspecialidad)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.CatEspecialidadId)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_ESPECIALIDAD");

                entity.HasOne(d => d.CatEstatusSolicitud)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.CatEstatusSolicitudId)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_ESTATUS_SOLICITUD");

                entity.HasOne(d => d.CatTipoPersonafisica)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.CatTipoPersonafisicaId)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_TIPO_PERSONAFISICA");

                entity.HasOne(d => d.CatUbicaciongeografica)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.CatUbicaciongeograficaId)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_UBICACIONGEOGRAFICA");

                entity.HasOne(d => d.CatUsuarioModifica)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.CatUsuarioModificaId)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_USUARIO");

                entity.HasOne(d => d.IdEscolaridadNavigation)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.IdEscolaridad)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_ESCOLARIDAD");

                entity.HasOne(d => d.IdEstatusEscolaridadNavigation)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.IdEstatusEscolaridad)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_ESTATUSESTUDIOS");

                entity.HasOne(d => d.IdGeneroNavigation)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.IdGenero)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_CAT_GENERO");

                entity.HasOne(d => d.TblOrganismo)
                    .WithMany(p => p.TblFisicaSolicituds)
                    .HasForeignKey(d => d.TblOrganismoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_TBL_ORGANISMOS");
            });

            modelBuilder.Entity<TblFisicaSolicitudDocumento>(entity =>
            {
                entity.ToTable("TBL_FISICA_SOLICITUD_DOCUMENTOS");

                entity.Property(e => e.Inclusion).HasColumnType("datetime");

                entity.Property(e => e.Nombre).HasMaxLength(600);

                entity.Property(e => e.TblFisicaSolicitudId).HasColumnName("TBL_FISICA_SOLICITUD_ID");

                entity.Property(e => e.Token).HasMaxLength(600);

                entity.HasOne(d => d.TblFisicaSolicitud)
                    .WithMany(p => p.TblFisicaSolicitudDocumentos)
                    .HasForeignKey(d => d.TblFisicaSolicitudId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TBL_FISICA_SOLICITUD_DOCUMENTOS_TBL_FISICA_SOLICITUD");
            });

            modelBuilder.Entity<TblLogMovUsuario>(entity =>
            {
                entity.HasKey(e => e.TlmuId)
                    .HasName("PK__TBL_LOG___7AB20BA354FBF4C4");

                entity.ToTable("TBL_LOG_MOV_USUARIO");

                entity.Property(e => e.TlmuId)
                    .ValueGeneratedNever()
                    .HasColumnName("tlmu_id");

                entity.Property(e => e.TlmuJsonCambio).HasColumnName("tlmu_json_cambio");

                entity.Property(e => e.TmuId).HasColumnName("tmu_id");

                entity.HasOne(d => d.Tmu)
                    .WithMany(p => p.TblLogMovUsuarios)
                    .HasForeignKey(d => d.TmuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_LOG_M__tmu_i__5AEE82B9");
            });

            modelBuilder.Entity<TblLogSistema>(entity =>
            {
                entity.ToTable("tbl_log_sistemas");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatAccionesSistemaId).HasColumnName("CAT_ACCIONES_SISTEMA_ID");

                entity.Property(e => e.CatModuloSistemaId).HasColumnName("CAT_MODULO_SISTEMA_ID");

                entity.Property(e => e.CatUsuarioId).HasColumnName("CAT_USUARIO_ID");

                entity.Property(e => e.Descripcion).HasMaxLength(300);

                entity.Property(e => e.Endpoint).HasMaxLength(300);

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Store).HasMaxLength(300);

                entity.HasOne(d => d.CatAccionesSistema)
                    .WithMany(p => p.TblLogSistemas)
                    .HasForeignKey(d => d.CatAccionesSistemaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tbl_log_s__CAT_A__6A1BB7B0");

                entity.HasOne(d => d.CatModuloSistema)
                    .WithMany(p => p.TblLogSistemas)
                    .HasForeignKey(d => d.CatModuloSistemaId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tbl_log_sistemas_CAT_MODULO_SISTEMA");

                entity.HasOne(d => d.CatUsuario)
                    .WithMany(p => p.TblLogSistemas)
                    .HasForeignKey(d => d.CatUsuarioId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tbl_log_s__CAT_U__6B0FDBE9");
            });

            modelBuilder.Entity<TblMovUsuario>(entity =>
            {
                entity.HasKey(e => e.TmuId)
                    .HasName("PK__TBL_MOV___3BCCD4703D11157A");

                entity.ToTable("TBL_MOV_USUARIO");

                entity.Property(e => e.TmuId)
                    .ValueGeneratedNever()
                    .HasColumnName("tmu_id");

                entity.Property(e => e.TcrId).HasColumnName("tcr_id");

                entity.Property(e => e.TmuFechaModificacion)
                    .HasColumnType("datetime")
                    .HasColumnName("tmu_fecha_modificacion");

                entity.Property(e => e.TmuUsIdResponsable).HasColumnName("tmu_us_id_responsable");

                entity.Property(e => e.UsCorreo)
                    .HasMaxLength(500)
                    .HasColumnName("us_correo");

                entity.Property(e => e.UsCuenta)
                    .HasMaxLength(500)
                    .HasColumnName("us_cuenta");

                entity.Property(e => e.UsExpediente).HasColumnName("us_expediente");

                entity.Property(e => e.UsId).HasColumnName("us_id");

                entity.Property(e => e.UsInclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("us_inclusion");

                entity.Property(e => e.UsMaterno)
                    .HasMaxLength(500)
                    .HasColumnName("us_materno");

                entity.Property(e => e.UsNombres)
                    .HasMaxLength(500)
                    .HasColumnName("us_nombres");

                entity.Property(e => e.UsPassword)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("us_password");

                entity.Property(e => e.UsPaterno)
                    .HasMaxLength(500)
                    .HasColumnName("us_paterno");

                entity.Property(e => e.UsPuesto)
                    .HasMaxLength(500)
                    .HasColumnName("us_puesto");

                entity.Property(e => e.UsesId).HasColumnName("uses_id");

                entity.HasOne(d => d.TmuUsIdResponsableNavigation)
                    .WithMany(p => p.TblMovUsuarios)
                    .HasForeignKey(d => d.TmuUsIdResponsable)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_MOV_U__tmu_u__5812160E");
            });

            modelBuilder.Entity<TblOrganismo>(entity =>
            {
                entity.ToTable("TBL_ORGANISMOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Activo).HasColumnName("activo");

                entity.Property(e => e.Calle)
                    .HasMaxLength(400)
                    .HasColumnName("calle");

                entity.Property(e => e.Cargo)
                    .HasMaxLength(400)
                    .HasColumnName("cargo");

                entity.Property(e => e.CatDeporteId).HasColumnName("CAT_DEPORTE_ID");

                entity.Property(e => e.CatTipoOrganismosId).HasColumnName("CAT_TIPO_ORGANISMOS_ID");

                entity.Property(e => e.CatUbicaciongeograficaId).HasColumnName("CAT_UBICACIONGEOGRAFICA_ID");

                entity.Property(e => e.Colonia)
                    .HasMaxLength(400)
                    .HasColumnName("colonia");

                entity.Property(e => e.ComentarioInactivo)
                    .HasMaxLength(500)
                    .HasColumnName("comentarioInactivo");

                entity.Property(e => e.Contacto)
                    .HasMaxLength(400)
                    .HasColumnName("contacto");

                entity.Property(e => e.ContactoTelefono)
                    .HasMaxLength(400)
                    .HasColumnName("contacto_telefono");

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .HasColumnName("cp");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(400)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Email)
                    .HasMaxLength(400)
                    .HasColumnName("email");

                entity.Property(e => e.FechaNac)
                    .HasColumnType("date")
                    .HasColumnName("fecha_nac");

                entity.Property(e => e.Foto)
                    .HasMaxLength(400)
                    .HasColumnName("foto");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NumExt)
                    .HasMaxLength(400)
                    .HasColumnName("num_ext");

                entity.Property(e => e.NumInt)
                    .HasMaxLength(400)
                    .HasColumnName("num_int");

                entity.Property(e => e.RelEstadosMunicipiosId).HasColumnName("REL_ESTADOS_MUNICIPIOS_ID");

                entity.Property(e => e.Rfc)
                    .HasMaxLength(400)
                    .HasColumnName("rfc");

                entity.Property(e => e.Talla).HasMaxLength(3);

                entity.Property(e => e.TblEstatusOrganismoId).HasColumnName("TBL_ESTATUS_ORGANISMO_ID");

                entity.Property(e => e.Telefonos)
                    .HasMaxLength(400)
                    .HasColumnName("telefonos");

                entity.Property(e => e.TitularApeMat)
                    .HasMaxLength(400)
                    .HasColumnName("titular_ape_mat");

                entity.Property(e => e.TitularApePat)
                    .HasMaxLength(400)
                    .HasColumnName("titular_ape_pat");

                entity.Property(e => e.TitularNombres)
                    .HasMaxLength(400)
                    .HasColumnName("titular_nombres");

                entity.Property(e => e.Titulo)
                    .HasMaxLength(400)
                    .HasColumnName("titulo");

                entity.Property(e => e.TokenLogo)
                    .HasMaxLength(600)
                    .HasColumnName("token_logo");

                entity.HasOne(d => d.CatDeporte)
                    .WithMany(p => p.TblOrganismos)
                    .HasForeignKey(d => d.CatDeporteId)
                    .HasConstraintName("FK_TBL_ORGANISMOS_CAT_DEPORTE");

                entity.HasOne(d => d.CatTipoOrganismos)
                    .WithMany(p => p.TblOrganismos)
                    .HasForeignKey(d => d.CatTipoOrganismosId)
                    .HasConstraintName("FK__TBL_ORGAN__CAT_T__02C769E9");

                entity.HasOne(d => d.CatUbicaciongeografica)
                    .WithMany(p => p.TblOrganismos)
                    .HasForeignKey(d => d.CatUbicaciongeograficaId)
                    .HasConstraintName("CAT_UBICACIONGEOGRAFICA_ID");

                entity.HasOne(d => d.TblEstatusOrganismo)
                    .WithMany(p => p.TblOrganismos)
                    .HasForeignKey(d => d.TblEstatusOrganismoId)
                    .HasConstraintName("FK__TBL_ORGAN__TBL_E__04AFB25B");
            });

            modelBuilder.Entity<TblOrganismosDirectivo>(entity =>
            {
                entity.ToTable("TBL_ORGANISMOS_DIRECTIVOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CargoDirectivo)
                    .HasMaxLength(200)
                    .HasColumnName("cargoDirectivo");

                entity.Property(e => e.CatCargoDirectivoId).HasColumnName("CAT_CARGO_DIRECTIVO_ID");

                entity.Property(e => e.Correo)
                    .HasMaxLength(400)
                    .HasColumnName("correo");

                entity.Property(e => e.Curp)
                    .HasMaxLength(20)
                    .HasColumnName("curp");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(500)
                    .HasColumnName("nombre");

                entity.Property(e => e.TblOrganismosDirecPerId).HasColumnName("TBL_ORGANISMOS_DIREC_PER_ID");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(200)
                    .HasColumnName("telefono");

                entity.HasOne(d => d.CatCargoDirectivo)
                    .WithMany(p => p.TblOrganismosDirectivos)
                    .HasForeignKey(d => d.CatCargoDirectivoId)
                    .HasConstraintName("FK_TBL_ORGANISMOS_DIRECTIVOS_CAT_CARGO_DIRECTIVO");

                entity.HasOne(d => d.TblOrganismosDirecPer)
                    .WithMany(p => p.TblOrganismosDirectivos)
                    .HasForeignKey(d => d.TblOrganismosDirecPerId)
                    .HasConstraintName("FK_TBL_ORGANISMOS_DIRECTIVOS_TBL_ORGANISMOS_DIRECTIVOSPERIODO");
            });

            modelBuilder.Entity<TblOrganismosDirectivosperiodo>(entity =>
            {
                entity.ToTable("TBL_ORGANISMOS_DIRECTIVOSPERIODO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FechaFin)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaFin");

                entity.Property(e => e.FechaInicio)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaInicio");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.TblOrganismosId).HasColumnName("TBL_ORGANISMOS_ID");

                entity.HasOne(d => d.TblOrganismos)
                    .WithMany(p => p.TblOrganismosDirectivosperiodos)
                    .HasForeignKey(d => d.TblOrganismosId)
                    .HasConstraintName("FK_TBL_ORGANISMOS_DIRECTIVOSPERIODO_TBL_ORGANISMOS");
            });

            modelBuilder.Entity<TblOrganismosDocumento>(entity =>
            {
                entity.ToTable("TBL_ORGANISMOS_DOCUMENTOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatEstatusDocumentosId).HasColumnName("CAT_ESTATUS_DOCUMENTOS_ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(600)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.NombreDocumento)
                    .HasMaxLength(600)
                    .HasColumnName("nombre_documento");

                entity.Property(e => e.TblOrganismosId).HasColumnName("TBL_ORGANISMOS_ID");

                entity.Property(e => e.TokenDocumento)
                    .HasMaxLength(600)
                    .HasColumnName("token_documento");

                entity.HasOne(d => d.CatEstatusDocumentos)
                    .WithMany(p => p.TblOrganismosDocumentos)
                    .HasForeignKey(d => d.CatEstatusDocumentosId)
                    .HasConstraintName("FK_TBL_ORGANISMOS_DOCUMENTOS_CAT_ESTATUS_DOCUMENTOS");

                entity.HasOne(d => d.TblOrganismos)
                    .WithMany(p => p.TblOrganismosDocumentos)
                    .HasForeignKey(d => d.TblOrganismosId)
                    .HasConstraintName("FK__TBL_ORGAN__TBL_O__1D4655FB");
            });

            modelBuilder.Entity<TblOrganismosSolicitud>(entity =>
            {
                entity.ToTable("TBL_ORGANISMOS_SOLICITUD");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Calle)
                    .HasMaxLength(400)
                    .HasColumnName("calle");

                entity.Property(e => e.Cargo)
                    .HasMaxLength(400)
                    .HasColumnName("cargo");

                entity.Property(e => e.CatTipoOrganismosId).HasColumnName("CAT_TIPO_ORGANISMOS_ID");

                entity.Property(e => e.CatUbicaciongeograficaId).HasColumnName("CAT_UBICACIONGEOGRAFICA_ID");

                entity.Property(e => e.Colonia)
                    .HasMaxLength(400)
                    .HasColumnName("colonia");

                entity.Property(e => e.Contacto)
                    .HasMaxLength(400)
                    .HasColumnName("contacto");

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .HasColumnName("cp");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(400)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Email)
                    .HasMaxLength(400)
                    .HasColumnName("email");

                entity.Property(e => e.FechaNac)
                    .HasColumnType("date")
                    .HasColumnName("fecha_nac");

                entity.Property(e => e.Foto)
                    .HasMaxLength(400)
                    .HasColumnName("foto");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.NumExt)
                    .HasMaxLength(400)
                    .HasColumnName("num_ext");

                entity.Property(e => e.NumInt)
                    .HasMaxLength(400)
                    .HasColumnName("num_int");

                entity.Property(e => e.Rfc)
                    .HasMaxLength(400)
                    .HasColumnName("rfc");

                entity.Property(e => e.TblEstatusOrganismoId).HasColumnName("TBL_ESTATUS_ORGANISMO_ID");

                entity.Property(e => e.Telefonos)
                    .HasMaxLength(400)
                    .HasColumnName("telefonos");

                entity.Property(e => e.TitularApeMat)
                    .HasMaxLength(400)
                    .HasColumnName("titular_ape_mat");

                entity.Property(e => e.TitularApePat)
                    .HasMaxLength(400)
                    .HasColumnName("titular_ape_pat");

                entity.Property(e => e.TitularNombres)
                    .HasMaxLength(400)
                    .HasColumnName("titular_nombres");

                entity.Property(e => e.Titulo)
                    .HasMaxLength(400)
                    .HasColumnName("titulo");

                entity.HasOne(d => d.CatTipoOrganismos)
                    .WithMany(p => p.TblOrganismosSolicituds)
                    .HasForeignKey(d => d.CatTipoOrganismosId)
                    .HasConstraintName("FK__TBL_ORGAN__CAT_T__1E3A7A34");

                entity.HasOne(d => d.CatUbicaciongeografica)
                    .WithMany(p => p.TblOrganismosSolicituds)
                    .HasForeignKey(d => d.CatUbicaciongeograficaId)
                    .HasConstraintName("FK__TBL_ORGAN__CAT_U__1F2E9E6D");

                entity.HasOne(d => d.TblEstatusOrganismo)
                    .WithMany(p => p.TblOrganismosSolicituds)
                    .HasForeignKey(d => d.TblEstatusOrganismoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_ORGAN__TBL_E__2022C2A6");
            });

            modelBuilder.Entity<TblPersonafisicaDocumento>(entity =>
            {
                entity.ToTable("TBL_PERSONAFISICA_DOCUMENTOS");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatPersonafisicaId).HasColumnName("CAT_PERSONAFISICA_ID");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.NombreDocumento)
                    .HasMaxLength(600)
                    .HasColumnName("nombre_documento");

                entity.Property(e => e.TokenDocumento)
                    .HasMaxLength(600)
                    .HasColumnName("token_documento");

                entity.HasOne(d => d.CatPersonafisica)
                    .WithMany(p => p.TblPersonafisicaDocumentos)
                    .HasForeignKey(d => d.CatPersonafisicaId)
                    .HasConstraintName("FK__TBL_PERSO__CAT_P__2116E6DF");
            });

            modelBuilder.Entity<TblRudFormato>(entity =>
            {
                entity.ToTable("TBL_RUD_FORMATO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Alias).HasMaxLength(1000);

                entity.Property(e => e.RutaFisica).HasMaxLength(400);
            });

            modelBuilder.Entity<TblRudFormatoCoordenada>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("TBL_RUD_FORMATO_COORDENADAS");

                entity.Property(e => e.Tag)
                    .HasMaxLength(300)
                    .HasColumnName("TAG");

                entity.Property(e => e.TblRudFormatoId).HasColumnName("TBL_RUD_FORMATO_ID");

                entity.Property(e => e.X).HasColumnName("x");

                entity.Property(e => e.Y).HasColumnName("y");

                entity.HasOne(d => d.TblRudFormato)
                    .WithMany()
                    .HasForeignKey(d => d.TblRudFormatoId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_RUD_F__TBL_R__220B0B18");
            });

            modelBuilder.Entity<TblRudOrganismo>(entity =>
            {
                entity.ToTable("TBL_RUD_ORGANISMO");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Expedicion).HasColumnType("datetime");

                entity.Property(e => e.Folio).HasMaxLength(50);

                entity.Property(e => e.Inscripcion).HasColumnType("datetime");

                entity.Property(e => e.Rud)
                    .HasMaxLength(640)
                    .HasColumnName("RUD");

                entity.Property(e => e.TblOrganismosId).HasColumnName("TBL_ORGANISMOS_ID");

                entity.Property(e => e.TblRudFormato).HasColumnName("TBL_RUD_FORMATO");

                entity.Property(e => e.Vencimiento).HasColumnType("datetime");

                entity.HasOne(d => d.TblOrganismos)
                    .WithMany(p => p.TblRudOrganismos)
                    .HasForeignKey(d => d.TblOrganismosId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_RUD_O__TBL_O__28ED12D1");

                entity.HasOne(d => d.TblRudFormatoNavigation)
                    .WithMany(p => p.TblRudOrganismos)
                    .HasForeignKey(d => d.TblRudFormato)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_RUD_O__TBL_R__23F3538A");
            });

            modelBuilder.Entity<Tblog>(entity =>
            {
                entity.ToTable("tblog");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CatSistemaId).HasColumnName("cat_sistema_id");

                entity.Property(e => e.DescripcionCorta).HasColumnName("Descripcion_corta");

                entity.Property(e => e.IdAlfa)
                    .HasMaxLength(2000)
                    .HasColumnName("Id_ALFA");

                entity.Property(e => e.IdNumerico).HasColumnName("Id_NUMERICO");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Segmento).HasMaxLength(400);

                entity.Property(e => e.Sp)
                    .HasMaxLength(400)
                    .HasColumnName("SP");

                entity.HasOne(d => d.CatSistema)
                    .WithMany(p => p.Tblogs)
                    .HasForeignKey(d => d.CatSistemaId)
                    .HasConstraintName("FK__tblog__cat_siste__02FC7413");
            });

            modelBuilder.Entity<ViewCatPersonafisica>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VIEW_CAT_PERSONAFISICA");

                entity.Property(e => e.Calle)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CatUbicaciongeograficaId).HasColumnName("CAT_UBICACIONGEOGRAFICA_ID");

                entity.Property(e => e.Ciudad).HasMaxLength(500);

                entity.Property(e => e.CodigoPostal).HasMaxLength(5);

                entity.Property(e => e.Colonia).HasMaxLength(500);

                entity.Property(e => e.Curp)
                    .HasMaxLength(18)
                    .IsUnicode(false)
                    .HasColumnName("CURP");

                entity.Property(e => e.Estado).HasMaxLength(500);

                entity.Property(e => e.FechaModificacion).HasColumnType("datetime");

                entity.Property(e => e.FechaNacimiento).HasColumnType("date");

                entity.Property(e => e.FechaRegistro).HasColumnType("datetime");

                entity.Property(e => e.IdEscolaridad).HasColumnName("idEscolaridad");

                entity.Property(e => e.IdEscolaridadAlfa)
                    .HasMaxLength(300)
                    .HasColumnName("idEscolaridad_ALFA");

                entity.Property(e => e.IdEstatusEscolaridad).HasColumnName("idEstatusEscolaridad");

                entity.Property(e => e.IdEstatusEscolaridadAlfa)
                    .HasMaxLength(300)
                    .HasColumnName("idEstatusEscolaridad_ALFA");

                entity.Property(e => e.IdGenero).HasColumnName("idGenero");

                entity.Property(e => e.IdGeneroAlfa)
                    .HasMaxLength(300)
                    .HasColumnName("idGenero_ALFA");

                entity.Property(e => e.IdPersona).HasColumnName("idPersona");

                entity.Property(e => e.Materno)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Municipio).HasMaxLength(500);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NumExt)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("Num_Ext");

                entity.Property(e => e.NumInt)
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("Num_Int");

                entity.Property(e => e.NumeroIdentificacion).HasMaxLength(300);

                entity.Property(e => e.Paterno)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TipoIdentificacion).HasMaxLength(300);
            });

            modelBuilder.Entity<ViewCatUsuario>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VIEW_CAT_USUARIO");

                entity.Property(e => e.EnvioToken)
                    .HasColumnType("datetime")
                    .HasColumnName("envio_token");

                entity.Property(e => e.Estatus)
                    .HasMaxLength(500)
                    .HasColumnName("estatus");

                entity.Property(e => e.IdArea).HasColumnName("idArea");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.NombreCompleto).HasMaxLength(1502);

                entity.Property(e => e.TcrId).HasColumnName("tcr_id");

                entity.Property(e => e.TcrTipo)
                    .HasMaxLength(500)
                    .HasColumnName("tcr_tipo");

                entity.Property(e => e.UsCorreo)
                    .HasMaxLength(500)
                    .HasColumnName("us_correo");

                entity.Property(e => e.UsCuenta)
                    .HasMaxLength(500)
                    .HasColumnName("us_cuenta");

                entity.Property(e => e.UsExpediente).HasColumnName("us_expediente");

                entity.Property(e => e.UsId).HasColumnName("us_id");

                entity.Property(e => e.UsIntentos).HasColumnName("us_intentos");

                entity.Property(e => e.UsMaterno)
                    .HasMaxLength(500)
                    .HasColumnName("us_materno");

                entity.Property(e => e.UsNombres)
                    .HasMaxLength(500)
                    .HasColumnName("us_nombres");

                entity.Property(e => e.UsPassword)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("us_password");

                entity.Property(e => e.UsPaterno)
                    .HasMaxLength(500)
                    .HasColumnName("us_paterno");

                entity.Property(e => e.UsPuesto)
                    .HasMaxLength(500)
                    .HasColumnName("us_puesto");

                entity.Property(e => e.UsesId).HasColumnName("uses_id");
            });

            modelBuilder.Entity<ViewFederacione>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VIEW_FEDERACIONES");

                entity.Property(e => e.IdFederacion).HasColumnName("id_federacion");

                entity.Property(e => e.IdUsuario).HasColumnName("id_usuario");

                entity.Property(e => e.Titulo)
                    .HasMaxLength(400)
                    .HasColumnName("titulo");

                entity.Property(e => e.UsCorreo)
                    .HasMaxLength(500)
                    .HasColumnName("us_correo");

                entity.Property(e => e.UsCuenta)
                    .HasMaxLength(500)
                    .HasColumnName("us_cuenta");

                entity.Property(e => e.UsMaterno)
                    .HasMaxLength(500)
                    .HasColumnName("us_materno");

                entity.Property(e => e.UsNombres)
                    .HasMaxLength(500)
                    .HasColumnName("us_nombres");

                entity.Property(e => e.UsPaterno)
                    .HasMaxLength(500)
                    .HasColumnName("us_paterno");
            });

            modelBuilder.Entity<ViewTblOrganismo>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("VIEW_TBL_ORGANISMOS");

                entity.Property(e => e.Calle)
                    .HasMaxLength(400)
                    .HasColumnName("calle");

                entity.Property(e => e.Cargo)
                    .HasMaxLength(400)
                    .HasColumnName("cargo");

                entity.Property(e => e.CatTipoOrganismosAlfa)
                    .IsUnicode(false)
                    .HasColumnName("CAT_TIPO_ORGANISMOS_ALFA");

                entity.Property(e => e.CatTipoOrganismosId).HasColumnName("CAT_TIPO_ORGANISMOS_ID");

                entity.Property(e => e.CatUbicaciongeograficaId).HasColumnName("CAT_UBICACIONGEOGRAFICA_ID");

                entity.Property(e => e.Colonia).HasMaxLength(500);

                entity.Property(e => e.Contacto)
                    .HasMaxLength(400)
                    .HasColumnName("contacto");

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .HasColumnName("cp");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(400)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Email)
                    .HasMaxLength(400)
                    .HasColumnName("email");

                entity.Property(e => e.Estado).HasMaxLength(500);

                entity.Property(e => e.FechaNac)
                    .HasColumnType("date")
                    .HasColumnName("fecha_nac");

                entity.Property(e => e.Foto)
                    .HasMaxLength(400)
                    .HasColumnName("foto");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion");

                entity.Property(e => e.Municipio).HasMaxLength(500);

                entity.Property(e => e.NumExt)
                    .HasMaxLength(400)
                    .HasColumnName("num_ext");

                entity.Property(e => e.NumInt)
                    .HasMaxLength(400)
                    .HasColumnName("num_int");

                entity.Property(e => e.Rfc)
                    .HasMaxLength(400)
                    .HasColumnName("rfc");

                entity.Property(e => e.Rud)
                    .HasMaxLength(640)
                    .HasColumnName("RUD");

                entity.Property(e => e.Talla).HasMaxLength(3);

                entity.Property(e => e.TblEstatusOrganismoId).HasColumnName("TBL_ESTATUS_ORGANISMO_ID");

                entity.Property(e => e.TblEstatusOrganismoIdAlfa)
                    .HasMaxLength(400)
                    .HasColumnName("TBL_ESTATUS_ORGANISMO_ID_ALFA");

                entity.Property(e => e.Telefonos)
                    .HasMaxLength(400)
                    .HasColumnName("telefonos");

                entity.Property(e => e.TitularApeMat)
                    .HasMaxLength(400)
                    .HasColumnName("titular_ape_mat");

                entity.Property(e => e.TitularApePat)
                    .HasMaxLength(400)
                    .HasColumnName("titular_ape_pat");

                entity.Property(e => e.TitularNombres)
                    .HasMaxLength(400)
                    .HasColumnName("titular_nombres");

                entity.Property(e => e.Titulo)
                    .HasMaxLength(400)
                    .HasColumnName("titulo");
            });

            modelBuilder.Entity<VwRandom>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vwRandom");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

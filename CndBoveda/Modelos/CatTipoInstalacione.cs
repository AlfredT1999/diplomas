﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatTipoInstalacione
    {
        public CatTipoInstalacione()
        {
            CatTipoInstalacionSedes = new HashSet<CatTipoInstalacionSede>();
        }

        public int Id { get; set; }
        public string Instalacion { get; set; } = null!;
        public bool? Activo { get; set; }

        public virtual ICollection<CatTipoInstalacionSede> CatTipoInstalacionSedes { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatEstatusDocumento
    {
        public CatEstatusDocumento()
        {
            TblOrganismosDocumentos = new HashSet<TblOrganismosDocumento>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public DateTime Inclusion { get; set; }
        public bool Activo { get; set; }

        public virtual ICollection<TblOrganismosDocumento> TblOrganismosDocumentos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatTipoRegiman
    {
        public CatTipoRegiman()
        {
            CatUsuarios = new HashSet<CatUsuario>();
        }

        public int TcrId { get; set; }
        public string TcrTipo { get; set; } = null!;

        public virtual ICollection<CatUsuario> CatUsuarios { get; set; }
    }
}

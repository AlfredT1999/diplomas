﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatDisciplina
    {
        public CatDisciplina()
        {
            RelDepDiscEsps = new HashSet<RelDepDiscEsp>();
            TblAfiliacionClubs = new HashSet<TblAfiliacionClub>();
            TblFisicaSolicituds = new HashSet<TblFisicaSolicitud>();
        }

        public int Id { get; set; }
        public string Disciplina { get; set; } = null!;
        public DateTime? Inclusion { get; set; }
        public bool? Activo { get; set; }
        public int? CatDeporteId { get; set; }

        public virtual CatDeporte? CatDeporte { get; set; }
        public virtual ICollection<RelDepDiscEsp> RelDepDiscEsps { get; set; }
        public virtual ICollection<TblAfiliacionClub> TblAfiliacionClubs { get; set; }
        public virtual ICollection<TblFisicaSolicitud> TblFisicaSolicituds { get; set; }
    }
}

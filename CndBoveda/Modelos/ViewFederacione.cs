﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class ViewFederacione
    {
        public int IdUsuario { get; set; }
        public int IdFederacion { get; set; }
        public string? Titulo { get; set; }
        public string UsNombres { get; set; } = null!;
        public string UsPaterno { get; set; } = null!;
        public string UsMaterno { get; set; } = null!;
        public string UsCuenta { get; set; } = null!;
        public string UsCorreo { get; set; } = null!;
    }
}

﻿using CndDiplomas;

namespace Execute
{
    public class MsgError
    {
        public const String Add = "Error al agregar elemento";
        public const String Update = "Error al actualizar elemento";
        public const String Status = "Error al actualizar estatus del elemento";
        public const String Select = "Error al listar información";
        public const String Remove = "Error al Eliminar elemento";
        public const String RemoveRange = "Error al Eliminar elementos";
        public const String Search = "Elemento no encontrado";
        public const String EmptyList = "Sin elementos para mostrar";
        public const String EmptyDelegacion = "No se encontró delegación activa";
    }

    public static class Error
    {
        public static Respuesta Set(String tipe, String Error)
        {
            Respuesta Respuesta_ = new()
            {
                Exito = false,
                Error = Error,
                Mensaje = tipe
            };

            return Respuesta_;
        }

        public static Respuesta Set(String tipe, String Error, int id)
        {
            Respuesta Respuesta_ = new()
            {
                Exito = false,
                Error = Error,
                Mensaje = tipe,
                Id = id
            };

            return Respuesta_;
        }
    }
}

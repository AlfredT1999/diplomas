﻿#nullable disable

namespace CndDiplomas.ComplexModelos
{
    public class PreRegistroAprobacionDTO
    {
        public int TblPreregistroId { get; set; }
        public int BovedaCatUsuarioId { get; set; }
        public bool Estatus { get; set; }
        public string Justificacion { get; set; }
    }
}

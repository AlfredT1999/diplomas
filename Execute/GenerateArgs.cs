﻿using CndDiplomas;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Execute
{
    public class GenerateArgs
    {
        public static Object ExecuteSpNeutro(String SP, Object obj_)
        {
            CndDiplomas.Modelos.CndDiplomasContext ctx = new();
            var param = new SqlParameter[] {
                        new SqlParameter() {
                            ParameterName = "@INPUT",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = -1,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = JsonConvert.SerializeObject(obj_)
                        }
            };
            string json = JsonConvert.SerializeObject(obj_);
            String valor = SP + " @INPUT";
            List<CndDiplomas.Modelos.Respuestum> Response_ = ctx.Respuesta.FromSqlRaw(valor, param).AsEnumerable<CndDiplomas.Modelos.Respuestum>().
                ToList<CndDiplomas.Modelos.Respuestum>();
            CndDiplomas.Modelos.Respuestum Res_ = Response_.First();
            Respuesta ResponseDB = new Respuesta();
            if (Res_.Mensaje != String.Empty && Res_.Mensaje != null)
            {
                ResponseDB = (JsonConvert.DeserializeObject<List<Respuesta>>(Response_.First().Mensaje)).First();
            }
            else
            {
                ResponseDB.Exito = Res_.Exito == null ? false : (Boolean)Res_.Exito;
                ResponseDB.Error = Res_.Error;
                ResponseDB.Anotacion = Res_.Anotacion;
                ResponseDB.Mensaje = Res_.Mensaje;
                ResponseDB.Id = Res_.Id;
            }
            return ResponseDB;
        }

        public static Object ExecuteSp(String SP, Object obj_)
        {
            CndDiplomas.Modelos.CndDiplomasContext ctx = new CndDiplomas.Modelos.CndDiplomasContext();
            var param = new SqlParameter[] {
                        new SqlParameter() {
                            ParameterName = "@INPUT",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = -1,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = JsonConvert.SerializeObject(obj_)
                        }
            };
            string json = JsonConvert.SerializeObject(obj_);
            String valor = SP + " @INPUT";
            List<CndDiplomas.Modelos.Respuestum> Response_ = ctx.Respuesta.FromSqlRaw(valor, param).AsEnumerable<CndDiplomas.Modelos.Respuestum>().
                ToList<CndDiplomas.Modelos.Respuestum>();
            CndDiplomas.Modelos.Respuestum Res_ = Response_.First();
            Respuesta ResponseDB = new Respuesta();
            if (Res_.Mensaje != String.Empty && Res_.Mensaje != null)
            {
                try
                {
                    ResponseDB = (JsonConvert.DeserializeObject<List<Respuesta>>(Response_.First().Mensaje)).First();
                }
                catch
                {
                    ResponseDB = new Respuesta
                    {
                        Anotacion = Response_.First().Anotacion,
                        Error = Response_.First().Error,
                        Exito = (Boolean)Response_.First().Exito,
                        Mensaje = Response_.First().Mensaje,
                        Id = Response_.First().Id
                    };
                }
            }
            else
            {
                ResponseDB.Exito = Res_.Exito == null ? false : (Boolean)Res_.Exito;
                ResponseDB.Error = Res_.Error;
                ResponseDB.Anotacion = Res_.Anotacion;
                ResponseDB.Mensaje = Res_.Mensaje;
                ResponseDB.Id = Res_.Id;
            }
            return ResponseDB;
        }

        public static Respuesta ExecuteSpGet(String SP, Object obj_, Type tipo)
        {
            CndDiplomas.Modelos.CndDiplomasContext ctx = new CndDiplomas.Modelos.CndDiplomasContext();
            var param = new SqlParameter[] {
                        new SqlParameter() {
                            ParameterName = "@INPUT",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Size = -1,
                            Direction = System.Data.ParameterDirection.Input,
                            Value = JsonConvert.SerializeObject(obj_)
                        },
                        new SqlParameter() {
                            ParameterName = "@OUTPUT",
                            SqlDbType =  System.Data.SqlDbType.NVarChar,
                            Direction = System.Data.ParameterDirection.Output,
                            Size = -1
                        }};
            string json = JsonConvert.SerializeObject(obj_);
            String valor = SP + " @INPUT, @OUTPUT";
            List<CndDiplomas.Modelos.Respuestum> Response_ = ctx.Respuesta.FromSqlRaw(valor, param).AsEnumerable<CndDiplomas.Modelos.Respuestum>().
                ToList<CndDiplomas.Modelos.Respuestum>();
            CndDiplomas.Modelos.Respuestum Res_ = Response_.First();
            Respuesta ResponseDB = new Respuesta();
            ResponseDB.Exito = true;
            ResponseDB.Error = String.Empty;
            ResponseDB.Anotacion = String.Empty;
            ResponseDB.Mensaje = String.Empty;
            ResponseDB.Id = 0;
            object Obj_ = JsonConvert.DeserializeObject(Res_.Mensaje, tipo);
            ResponseDB.Objeto = Obj_;
            return ResponseDB;
        }
    }
}


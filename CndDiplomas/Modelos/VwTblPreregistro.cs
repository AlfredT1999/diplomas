﻿using System;
using System.Collections.Generic;

namespace CndDiplomas.Modelos
{
    public partial class VwTblPreregistro
    {
        public string? Rfc { get; set; }
        public string Rud { get; set; } = null!;
        public string? RazonSocial { get; set; }
        public int IdDeporte { get; set; }
        public string Deporte { get; set; } = null!;
        public int IdUbicacion { get; set; }
        public string? Estado { get; set; }
        public string? Municipio { get; set; }
        public string? CodigoPostal { get; set; }
        public string Email { get; set; } = null!;
        public string NumExt { get; set; } = null!;
        public string? NumInt { get; set; }
        public string? ContactoTelefono { get; set; }
        public string? Telefonos { get; set; }
        public string Institucion { get; set; } = null!;
        public string? ObjetivoPrincipal { get; set; }
        public string Usuario { get; set; } = null!;
        public string Contraseña { get; set; } = null!;
        public string RepetirContraseña { get; set; } = null!;
    }
}

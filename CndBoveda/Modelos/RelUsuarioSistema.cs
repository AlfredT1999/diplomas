﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelUsuarioSistema
    {
        public int SusId { get; set; }
        public int? SusIdUsuario { get; set; }
        public int? SusIdSistema { get; set; }
        public bool? SusActivo { get; set; }

        public virtual CatSistema? SusIdSistemaNavigation { get; set; }
        public virtual CatUsuario? SusIdUsuarioNavigation { get; set; }
    }
}

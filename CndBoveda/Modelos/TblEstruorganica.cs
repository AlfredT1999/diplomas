﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblEstruorganica
    {
        public TblEstruorganica()
        {
            RelUsuarioEstruorganicas = new HashSet<RelUsuarioEstruorganica>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; } = null!;
        public int? CatEstruOrganicaClaveId { get; set; }
        public bool Activa { get; set; }

        public virtual CatEstruorganicaClave? CatEstruOrganicaClave { get; set; }
        public virtual ICollection<RelUsuarioEstruorganica> RelUsuarioEstruorganicas { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatInstalacione
    {
        public CatInstalacione()
        {
            RelSedeInstalaciones = new HashSet<RelSedeInstalacione>();
        }

        public int IdInstalacion { get; set; }
        public string Nombre { get; set; } = null!;
        public bool Activo { get; set; }

        public virtual ICollection<RelSedeInstalacione> RelSedeInstalaciones { get; set; }
    }
}

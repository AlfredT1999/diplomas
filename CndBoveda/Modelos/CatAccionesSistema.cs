﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatAccionesSistema
    {
        public CatAccionesSistema()
        {
            TblLogSistemas = new HashSet<TblLogSistema>();
        }

        public int Id { get; set; }
        public string Accion { get; set; } = null!;

        public virtual ICollection<TblLogSistema> TblLogSistemas { get; set; }
    }
}

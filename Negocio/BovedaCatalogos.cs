﻿using CndBoveda.Modelos;
using Execute;

namespace Negocio
{
    public class BovedaCatalogos
    {
        Object Response_ = new();
        readonly bovedaContext _boveda = new();

        public Object GetCatalogos(String accion)
        {
            try
            {
                switch (accion)
                {
                    case "organismos":
                        Response_ = _boveda.TblOrganismos.ToList();
                        break;
                    case "clavealfa_organismos":
                        Response_ = _boveda.TblClavealfaOrganismos.ToList();
                        break;
                    case "ubicaciongeografica":
                        Response_ = _boveda.CatUbicaciongeograficas.ToList();
                        break;
                    case "deportes":
                        Response_ = _boveda.CatDeportes.ToList();
                        break;
                }

                return Response_;
            }
            catch (Exception ex)
            {
                return Error.Set(MsgError.Select, ex.Message);
            }
        }
    }
}

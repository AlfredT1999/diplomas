﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatUsuario
    {
        public CatUsuario()
        {
            RelUsuarioEstruorganicas = new HashSet<RelUsuarioEstruorganica>();
            RelUsuarioSistemas = new HashSet<RelUsuarioSistema>();
            TblFisicaSolicituds = new HashSet<TblFisicaSolicitud>();
            TblLogSistemas = new HashSet<TblLogSistema>();
            TblMovUsuarios = new HashSet<TblMovUsuario>();
        }

        public int UsId { get; set; }
        public int UsExpediente { get; set; }
        public string? UsPassword { get; set; }
        public string UsNombres { get; set; } = null!;
        public string UsPaterno { get; set; } = null!;
        public string UsMaterno { get; set; } = null!;
        public string UsCuenta { get; set; } = null!;
        public string UsCorreo { get; set; } = null!;
        public string UsPuesto { get; set; } = null!;
        public int? TcrId { get; set; }
        public int? UsesId { get; set; }
        public DateTime? Inclusion { get; set; }
        public DateTime? EnvioToken { get; set; }
        public int? UsIntentos { get; set; }

        public virtual CatTipoRegiman? Tcr { get; set; }
        public virtual CatUsuarioEstatus? Uses { get; set; }
        public virtual ICollection<RelUsuarioEstruorganica> RelUsuarioEstruorganicas { get; set; }
        public virtual ICollection<RelUsuarioSistema> RelUsuarioSistemas { get; set; }
        public virtual ICollection<TblFisicaSolicitud> TblFisicaSolicituds { get; set; }
        public virtual ICollection<TblLogSistema> TblLogSistemas { get; set; }
        public virtual ICollection<TblMovUsuario> TblMovUsuarios { get; set; }
    }
}

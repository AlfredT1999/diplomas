﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelPersonasFisica
    {
        public int Id { get; set; }
        public int RelDepDiscEspId { get; set; }
        public int CatPersonafisicaId { get; set; }
        public int CatTipoPersonafisicaId { get; set; }

        public virtual CatPersonafisica CatPersonafisica { get; set; } = null!;
        public virtual CatTipoPersonafisica CatTipoPersonafisica { get; set; } = null!;
        public virtual RelDepDiscEsp RelDepDiscEsp { get; set; } = null!;
    }
}

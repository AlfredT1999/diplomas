﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatTipoPersonafisica
    {
        public CatTipoPersonafisica()
        {
            RelPersonasFisicas = new HashSet<RelPersonasFisica>();
            TblFisicaSolicituds = new HashSet<TblFisicaSolicitud>();
        }

        public int IdTipoPersonaFisica { get; set; }
        public string TipoPersonaFisica { get; set; } = null!;
        public DateTime Inclusión { get; set; }
        public bool Activo { get; set; }
        public bool? EsPersonalTecnico { get; set; }
        public bool OrganismoRequerido { get; set; }

        public virtual ICollection<RelPersonasFisica> RelPersonasFisicas { get; set; }
        public virtual ICollection<TblFisicaSolicitud> TblFisicaSolicituds { get; set; }
    }
}

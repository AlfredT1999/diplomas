﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatEspecialidad
    {
        public CatEspecialidad()
        {
            RelDepDiscEsps = new HashSet<RelDepDiscEsp>();
            TblFisicaSolicituds = new HashSet<TblFisicaSolicitud>();
        }

        public int Id { get; set; }
        public string Especialidad { get; set; } = null!;
        public DateTime? Inclusion { get; set; }
        public bool? Activo { get; set; }

        public virtual ICollection<RelDepDiscEsp> RelDepDiscEsps { get; set; }
        public virtual ICollection<TblFisicaSolicitud> TblFisicaSolicituds { get; set; }
    }
}

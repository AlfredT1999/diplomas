﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatUbicaciongeografica
    {
        public CatUbicaciongeografica()
        {
            CatPersonafisicas = new HashSet<CatPersonafisica>();
            TblFisicaSolicituds = new HashSet<TblFisicaSolicitud>();
            TblOrganismos = new HashSet<TblOrganismo>();
            TblOrganismosSolicituds = new HashSet<TblOrganismosSolicitud>();
        }

        public int Id { get; set; }
        public int? CatEstadoId { get; set; }
        public string? CodigoPostal { get; set; }
        public string? Colonia { get; set; }
        public string? Municipio { get; set; }
        public string? Estado { get; set; }
        public string? Ciudad { get; set; }
        public DateTime? Inclusion { get; set; }

        public virtual CatEstado? CatEstado { get; set; }
        public virtual ICollection<CatPersonafisica> CatPersonafisicas { get; set; }
        public virtual ICollection<TblFisicaSolicitud> TblFisicaSolicituds { get; set; }
        public virtual ICollection<TblOrganismo> TblOrganismos { get; set; }
        public virtual ICollection<TblOrganismosSolicitud> TblOrganismosSolicituds { get; set; }
    }
}

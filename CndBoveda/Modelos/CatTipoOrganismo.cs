﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatTipoOrganismo
    {
        public CatTipoOrganismo()
        {
            TblOrganismos = new HashSet<TblOrganismo>();
            TblOrganismosSolicituds = new HashSet<TblOrganismosSolicitud>();
        }

        public int Id { get; set; }
        public string? Descripcion { get; set; }
        public DateTime? Inclusion { get; set; }
        public bool? Renade { get; set; }
        public bool? Sinade { get; set; }

        public virtual ICollection<TblOrganismo> TblOrganismos { get; set; }
        public virtual ICollection<TblOrganismosSolicitud> TblOrganismosSolicituds { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblClavealfaPersonafisica
    {
        public int Id { get; set; }
        public int CatPersonafisicaId { get; set; }
        public int CatClavealfaEstatusId { get; set; }
        public string Alfa { get; set; } = null!;
        public DateTime Inclusion { get; set; }
        public DateTime? Inscripcion { get; set; }
        public DateTime? Expedicion { get; set; }
        public DateTime Expira { get; set; }
        public DateTime? Modificacion { get; set; }
        public string? Folio { get; set; }

        public virtual CatClavealfaEstatus CatClavealfaEstatus { get; set; } = null!;
        public virtual CatPersonafisica CatPersonafisica { get; set; } = null!;
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblFisicaSolicitud
    {
        public TblFisicaSolicitud()
        {
            TblFisicaSolicitudDocumentos = new HashSet<TblFisicaSolicitudDocumento>();
        }

        public int Id { get; set; }
        public string Paterno { get; set; } = null!;
        public string? Materno { get; set; }
        public string Nombre { get; set; } = null!;
        public string? Curp { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public int? IdGenero { get; set; }
        public bool? Extranjero { get; set; }
        public int? IdEscolaridad { get; set; }
        public int? IdEstatusEscolaridad { get; set; }
        public string? TipoIdentificacion { get; set; }
        public string? NumeroIdentificacion { get; set; }
        public string? Calle { get; set; }
        public string? NumExterior { get; set; }
        public string? NumInterior { get; set; }
        public int? CatUbicaciongeograficaId { get; set; }
        public int TblOrganismoId { get; set; }
        public int? CatDisciplinaId { get; set; }
        public int? CatEspecialidadId { get; set; }
        public int? CatTipoPersonafisicaId { get; set; }
        public string? TokenFoto { get; set; }
        public int? CatEstatusSolicitudId { get; set; }
        public DateTime? Inclusion { get; set; }
        public int? CatUsuarioModificaId { get; set; }
        public DateTime? Modifica { get; set; }

        public virtual CatDisciplina? CatDisciplina { get; set; }
        public virtual CatEspecialidad? CatEspecialidad { get; set; }
        public virtual CatEstatusSolicitud? CatEstatusSolicitud { get; set; }
        public virtual CatTipoPersonafisica? CatTipoPersonafisica { get; set; }
        public virtual CatUbicaciongeografica? CatUbicaciongeografica { get; set; }
        public virtual CatUsuario? CatUsuarioModifica { get; set; }
        public virtual CatEscolaridad? IdEscolaridadNavigation { get; set; }
        public virtual CatEstatusestudio? IdEstatusEscolaridadNavigation { get; set; }
        public virtual CatGenero? IdGeneroNavigation { get; set; }
        public virtual TblOrganismo TblOrganismo { get; set; } = null!;
        public virtual ICollection<TblFisicaSolicitudDocumento> TblFisicaSolicitudDocumentos { get; set; }
    }
}

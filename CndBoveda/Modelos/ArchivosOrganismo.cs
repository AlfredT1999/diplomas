﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class ArchivosOrganismo
    {
        public int Id { get; set; }
        public int CatArchivoOrganismoId { get; set; }
        public int TblOrganismoId { get; set; }
        public string TokenDocumento { get; set; } = null!;
        public string Anio { get; set; } = null!;
        public DateTime? Inclusion { get; set; }

        public virtual CatArchivoOrganismo CatArchivoOrganismo { get; set; } = null!;
    }
}

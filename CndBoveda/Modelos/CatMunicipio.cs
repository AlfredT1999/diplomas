﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatMunicipio
    {
        public CatMunicipio()
        {
            RelEstadosMunicipios = new HashSet<RelEstadosMunicipio>();
        }

        public int IdMunicipio { get; set; }
        public string Municipio { get; set; } = null!;

        public virtual ICollection<RelEstadosMunicipio> RelEstadosMunicipios { get; set; }
    }
}

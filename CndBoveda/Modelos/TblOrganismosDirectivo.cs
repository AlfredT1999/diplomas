﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblOrganismosDirectivo
    {
        public int Id { get; set; }
        public int? TblOrganismosDirecPerId { get; set; }
        public int? CatCargoDirectivoId { get; set; }
        public string? CargoDirectivo { get; set; }
        public string Nombre { get; set; } = null!;
        public string? Curp { get; set; }
        public string? Correo { get; set; }
        public string? Telefono { get; set; }
        public DateTime? Inclusion { get; set; }

        public virtual CatCargoDirectivo? CatCargoDirectivo { get; set; }
        public virtual TblOrganismosDirectivosperiodo? TblOrganismosDirecPer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndDiplomas.Modelos
{
    public partial class TblPreregistroAprobacion
    {
        public int Id { get; set; }
        public int TblPreregistroId { get; set; }
        public int BovedaCatUsuarioId { get; set; }
        public bool Estatus { get; set; }
        public string? Justificacion { get; set; }
        public DateTime Inclusion { get; set; }

        public virtual TblPreregistro TblPreregistro { get; set; } = null!;
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatEstado
    {
        public CatEstado()
        {
            CatUbicaciongeograficas = new HashSet<CatUbicaciongeografica>();
            RelEstadosMunicipios = new HashSet<RelEstadosMunicipio>();
        }

        public int IdEstado { get; set; }
        public string Estado { get; set; } = null!;

        public virtual ICollection<CatUbicaciongeografica> CatUbicaciongeograficas { get; set; }
        public virtual ICollection<RelEstadosMunicipio> RelEstadosMunicipios { get; set; }
    }
}

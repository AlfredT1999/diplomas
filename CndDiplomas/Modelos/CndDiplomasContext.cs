﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CndDiplomas.Modelos
{
    public partial class CndDiplomasContext : DbContext
    {
        public CndDiplomasContext()
        {
        }

        public CndDiplomasContext(DbContextOptions<CndDiplomasContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Respuestum> Respuesta { get; set; } = null!;
        public virtual DbSet<TblPreregistro> TblPreregistros { get; set; } = null!;
        public virtual DbSet<TblPreregistroAprobacion> TblPreregistroAprobacions { get; set; } = null!;
        public virtual DbSet<TblUsuarioAcceso> TblUsuarioAccesos { get; set; } = null!;
        public virtual DbSet<VwTblPreregistro> VwTblPreregistros { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=10.10.0.32\\MSSQLSERVER2017;user=juanma;password=123;database=CndDiplomas");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Respuestum>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Anotacion).HasMaxLength(600);

                entity.Property(e => e.Error).HasMaxLength(600);

                entity.Property(e => e.Mensaje).HasMaxLength(600);
            });

            modelBuilder.Entity<TblPreregistro>(entity =>
            {
                entity.ToTable("tbl_preregistro");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BovedaCatDeporteId).HasColumnName("boveda_cat_deporte_id");

                entity.Property(e => e.BovedaTblOrganismoPerfisicaId).HasColumnName("boveda_tbl_organismo_perfisica_id");

                entity.Property(e => e.Correo)
                    .HasMaxLength(50)
                    .HasColumnName("correo");

                entity.Property(e => e.Cp)
                    .HasMaxLength(5)
                    .HasColumnName("cp");

                entity.Property(e => e.Estado).HasMaxLength(200);

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Institucion)
                    .HasMaxLength(300)
                    .HasColumnName("institucion");

                entity.Property(e => e.Municipio).HasMaxLength(200);

                entity.Property(e => e.NumExterior).HasMaxLength(50);

                entity.Property(e => e.NumInterior).HasMaxLength(50);

                entity.Property(e => e.ObjetivoInstitucio).HasColumnName("objetivoInstitucio");

                entity.Property(e => e.Password)
                    .HasMaxLength(200)
                    .HasColumnName("password");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(30)
                    .HasColumnName("telefono");

                entity.Property(e => e.Tipopersona)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("tipopersona")
                    .IsFixedLength();

                entity.Property(e => e.Usuario)
                    .HasMaxLength(200)
                    .HasColumnName("usuario");
            });

            modelBuilder.Entity<TblPreregistroAprobacion>(entity =>
            {
                entity.ToTable("tbl_preregistro_aprobacion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BovedaCatUsuarioId).HasColumnName("boveda_cat_usuario_id");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.TblPreregistroId).HasColumnName("tbl_preregistro_id");

                entity.HasOne(d => d.TblPreregistro)
                    .WithMany(p => p.TblPreregistroAprobacions)
                    .HasForeignKey(d => d.TblPreregistroId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tbl_prere__tbl_p__534D60F1");
            });

            modelBuilder.Entity<TblUsuarioAcceso>(entity =>
            {
                entity.ToTable("tbl_usuario_acceso");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BovedaTblOrganismoPerfisicaId).HasColumnName("boveda_tbl_organismo_perfisica_id");

                entity.Property(e => e.Contrasena)
                    .HasMaxLength(600)
                    .HasColumnName("contrasena");

                entity.Property(e => e.Inclusion)
                    .HasColumnType("datetime")
                    .HasColumnName("inclusion")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Tipopersona)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasColumnName("tipopersona")
                    .IsFixedLength();

                entity.Property(e => e.Usuario)
                    .HasMaxLength(600)
                    .HasColumnName("usuario");
            });

            modelBuilder.Entity<VwTblPreregistro>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_tbl_preregistro");

                entity.Property(e => e.CodigoPostal).HasMaxLength(5);

                entity.Property(e => e.ContactoTelefono)
                    .HasMaxLength(400)
                    .HasColumnName("contacto_telefono");

                entity.Property(e => e.Contraseña).HasColumnName("contraseña");

                entity.Property(e => e.Deporte).HasMaxLength(400);

                entity.Property(e => e.Email)
                    .HasMaxLength(400)
                    .HasColumnName("email");

                entity.Property(e => e.Estado).HasMaxLength(500);

                entity.Property(e => e.IdDeporte).HasColumnName("id_deporte");

                entity.Property(e => e.IdUbicacion).HasColumnName("id_ubicacion");

                entity.Property(e => e.Institucion).HasColumnName("institucion");

                entity.Property(e => e.Municipio).HasMaxLength(500);

                entity.Property(e => e.NumExt)
                    .HasMaxLength(400)
                    .HasColumnName("num_ext");

                entity.Property(e => e.NumInt)
                    .HasMaxLength(400)
                    .HasColumnName("num_int");

                entity.Property(e => e.ObjetivoPrincipal).HasColumnName("objetivo_principal");

                entity.Property(e => e.RazonSocial)
                    .HasMaxLength(400)
                    .HasColumnName("razon_social");

                entity.Property(e => e.RepetirContraseña).HasColumnName("repetir_contraseña");

                entity.Property(e => e.Rfc)
                    .HasMaxLength(400)
                    .HasColumnName("rfc");

                entity.Property(e => e.Rud)
                    .HasMaxLength(500)
                    .HasColumnName("RUD");

                entity.Property(e => e.Telefonos)
                    .HasMaxLength(400)
                    .HasColumnName("telefonos");

                entity.Property(e => e.Usuario).HasColumnName("usuario");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

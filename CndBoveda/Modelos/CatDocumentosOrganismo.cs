﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatDocumentosOrganismo
    {
        public int Id { get; set; }
        public string Nombre { get; set; } = null!;
        public string? Siglas { get; set; }
        public bool? Activo { get; set; }
    }
}

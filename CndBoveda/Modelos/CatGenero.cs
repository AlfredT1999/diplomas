﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatGenero
    {
        public CatGenero()
        {
            TblFisicaSolicituds = new HashSet<TblFisicaSolicitud>();
        }

        public int CatId { get; set; }
        public string CatName { get; set; } = null!;
        public bool Activo { get; set; }

        public virtual ICollection<TblFisicaSolicitud> TblFisicaSolicituds { get; set; }
    }
}

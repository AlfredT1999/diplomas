﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblFisicaSolicitudDocumento
    {
        public int Id { get; set; }
        public int TblFisicaSolicitudId { get; set; }
        public string Token { get; set; } = null!;
        public string Nombre { get; set; } = null!;
        public DateTime? Inclusion { get; set; }

        public virtual TblFisicaSolicitud TblFisicaSolicitud { get; set; } = null!;
    }
}

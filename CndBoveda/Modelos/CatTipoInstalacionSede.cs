﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatTipoInstalacionSede
    {
        public int Id { get; set; }
        public int? CatTipoInstalacionesId { get; set; }
        public int? CatSedeId { get; set; }
        public bool? Activo { get; set; }

        public virtual CatSede? CatSede { get; set; }
        public virtual CatTipoInstalacione? CatTipoInstalaciones { get; set; }
    }
}

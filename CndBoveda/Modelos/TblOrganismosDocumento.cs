﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblOrganismosDocumento
    {
        public int Id { get; set; }
        public int? TblOrganismosId { get; set; }
        public string? TokenDocumento { get; set; }
        public string NombreDocumento { get; set; } = null!;
        public string? Descripcion { get; set; }
        public int? CatEstatusDocumentosId { get; set; }
        public DateTime? Inclusion { get; set; }

        public virtual CatEstatusDocumento? CatEstatusDocumentos { get; set; }
        public virtual TblOrganismo? TblOrganismos { get; set; }
    }
}

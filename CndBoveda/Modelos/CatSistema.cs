﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatSistema
    {
        public CatSistema()
        {
            CatModuloSistemas = new HashSet<CatModuloSistema>();
            RelUsuarioSistemas = new HashSet<RelUsuarioSistema>();
            Tblogs = new HashSet<Tblog>();
        }

        public int SId { get; set; }
        public string? SSiatema { get; set; }
        public string? SDescripcion { get; set; }

        public virtual ICollection<CatModuloSistema> CatModuloSistemas { get; set; }
        public virtual ICollection<RelUsuarioSistema> RelUsuarioSistemas { get; set; }
        public virtual ICollection<Tblog> Tblogs { get; set; }
    }
}

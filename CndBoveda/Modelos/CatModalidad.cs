﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatModalidad
    {
        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public bool Activo { get; set; }
        public DateTime Incluision { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatDeporte
    {
        public CatDeporte()
        {
            CatDisciplinas = new HashSet<CatDisciplina>();
            RelDepDiscEsps = new HashSet<RelDepDiscEsp>();
            RelOrganismosDeportes = new HashSet<RelOrganismosDeporte>();
            TblAfiliacionClubs = new HashSet<TblAfiliacionClub>();
            TblOrganismos = new HashSet<TblOrganismo>();
        }

        public int Id { get; set; }
        public string Deporte { get; set; } = null!;
        public DateTime Inclusion { get; set; }
        public bool? Activo { get; set; }
        public bool Olimpico { get; set; }

        public virtual ICollection<CatDisciplina> CatDisciplinas { get; set; }
        public virtual ICollection<RelDepDiscEsp> RelDepDiscEsps { get; set; }
        public virtual ICollection<RelOrganismosDeporte> RelOrganismosDeportes { get; set; }
        public virtual ICollection<TblAfiliacionClub> TblAfiliacionClubs { get; set; }
        public virtual ICollection<TblOrganismo> TblOrganismos { get; set; }
    }
}

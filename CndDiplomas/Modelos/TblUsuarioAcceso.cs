﻿using System;
using System.Collections.Generic;

namespace CndDiplomas.Modelos
{
    public partial class TblUsuarioAcceso
    {
        public int Id { get; set; }
        public string Usuario { get; set; } = null!;
        public string Contrasena { get; set; } = null!;
        public int BovedaTblOrganismoPerfisicaId { get; set; }
        public string? Tipopersona { get; set; }
        public DateTime Inclusion { get; set; }
    }
}

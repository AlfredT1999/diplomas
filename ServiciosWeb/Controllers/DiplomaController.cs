﻿using CndDiplomas;
using CndDiplomas.ComplexModelos;
using Execute.Captcha;
using Microsoft.AspNetCore.Mvc;
using Negocio;

namespace ServiciosWeb.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DiplomaController : ControllerBase
    {
        private readonly ISignupService signupService;

        public DiplomaController(ISignupService signupService)
        {
            this.signupService = signupService;
        }

        [HttpGet]
        [Route("[action]")]
        public Object GetAllPreregistros()
        {
            return DiplomaNegocio.GetAllPreregistros();
        }

        [HttpPost]
        [Route("[action]")]
        public Respuesta Preregistro([FromQuery] PreRegistroDTO data)
        {
            return DiplomaNegocio.Preregistro(data);
        }

        [HttpPost]
        [Route("[action]")]
        public Respuesta AprobacionPreregistro([FromQuery] PreRegistroAprobacionDTO data)
        {
            return DiplomaNegocio.AprobacionPreregistro(data);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblRudOrganismo
    {
        public int Id { get; set; }
        public string Rud { get; set; } = null!;
        public int TblOrganismosId { get; set; }
        public DateTime Inscripcion { get; set; }
        public DateTime Expedicion { get; set; }
        public DateTime Vencimiento { get; set; }
        public string CodigoVerificacion { get; set; } = null!;
        public string Folio { get; set; } = null!;
        public Guid Semilla { get; set; }
        public int TblRudFormato { get; set; }

        public virtual TblOrganismo TblOrganismos { get; set; } = null!;
        public virtual TblRudFormato TblRudFormatoNavigation { get; set; } = null!;
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblLogSistema
    {
        public int Id { get; set; }
        public int CatModuloSistemaId { get; set; }
        public int CatAccionesSistemaId { get; set; }
        public int CatUsuarioId { get; set; }
        public int? Entidad { get; set; }
        public string Descripcion { get; set; } = null!;
        public string? Endpoint { get; set; }
        public string? Store { get; set; }
        public DateTime Inclusion { get; set; }

        public virtual CatAccionesSistema CatAccionesSistema { get; set; } = null!;
        public virtual CatModuloSistema CatModuloSistema { get; set; } = null!;
        public virtual CatUsuario CatUsuario { get; set; } = null!;
    }
}

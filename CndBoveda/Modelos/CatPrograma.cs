﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatPrograma
    {
        public int Id { get; set; }
        public string Programa { get; set; } = null!;
        public bool Activo { get; set; }
        public DateTime Inclusion { get; set; }
    }
}

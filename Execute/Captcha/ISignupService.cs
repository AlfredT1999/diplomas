﻿using CndDiplomas.ComplexModelos;

namespace Execute.Captcha
{
    public interface ISignupService
    {
        Task<SignupResponse> Signup(PreRegistroDTO signupRequest);
    }
}

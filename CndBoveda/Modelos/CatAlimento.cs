﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatAlimento
    {
        public int Id { get; set; }
        public string Alimento { get; set; } = null!;
        public bool? Activo { get; set; }
        public TimeSpan? Inicio { get; set; }
        public TimeSpan? Fin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndDiplomas.Modelos
{
    public partial class TblPreregistro
    {
        public TblPreregistro()
        {
            TblPreregistroAprobacions = new HashSet<TblPreregistroAprobacion>();
        }

        public int Id { get; set; }
        public int BovedaTblOrganismoPerfisicaId { get; set; }
        public string? Tipopersona { get; set; }
        public string Institucion { get; set; } = null!;
        public string Usuario { get; set; } = null!;
        public string Password { get; set; } = null!;
        public int? BovedaCatDeporteId { get; set; }
        public string ObjetivoInstitucio { get; set; } = null!;
        public string Cp { get; set; } = null!;
        public string Estado { get; set; } = null!;
        public string Municipio { get; set; } = null!;
        public string NumExterior { get; set; } = null!;
        public string? NumInterior { get; set; }
        public string Telefono { get; set; } = null!;
        public string Correo { get; set; } = null!;
        public DateTime Inclusion { get; set; }

        public virtual ICollection<TblPreregistroAprobacion> TblPreregistroAprobacions { get; set; }
    }
}

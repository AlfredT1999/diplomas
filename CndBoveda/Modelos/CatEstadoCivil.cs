﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatEstadoCivil
    {
        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public bool Activo { get; set; }
        public DateTime? Inclusion { get; set; }
    }
}

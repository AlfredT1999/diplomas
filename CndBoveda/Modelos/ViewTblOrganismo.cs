﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class ViewTblOrganismo
    {
        public int Id { get; set; }
        public string? Descripcion { get; set; }
        public int? CatTipoOrganismosId { get; set; }
        public string? CatTipoOrganismosAlfa { get; set; }
        public string? Titulo { get; set; }
        public string? Cargo { get; set; }
        public string? Calle { get; set; }
        public string? NumExt { get; set; }
        public string? NumInt { get; set; }
        public string? Estado { get; set; }
        public string? Municipio { get; set; }
        public string? Colonia { get; set; }
        public int? CatUbicaciongeograficaId { get; set; }
        public string? Cp { get; set; }
        public string? Rfc { get; set; }
        public string? Telefonos { get; set; }
        public string? Email { get; set; }
        public string? Contacto { get; set; }
        public string? Foto { get; set; }
        public string? TitularNombres { get; set; }
        public string? TitularApePat { get; set; }
        public string? TitularApeMat { get; set; }
        public DateTime? FechaNac { get; set; }
        public DateTime? Inclusion { get; set; }
        public int TblEstatusOrganismoId { get; set; }
        public string? TblEstatusOrganismoIdAlfa { get; set; }
        public string? Rud { get; set; }
        public string? Talla { get; set; }
    }
}

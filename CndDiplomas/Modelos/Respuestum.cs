﻿using System;
using System.Collections.Generic;

namespace CndDiplomas.Modelos
{
    public partial class Respuestum
    {
        public int Id { get; set; }
        public bool? Exito { get; set; }
        public string? Mensaje { get; set; }
        public string? Error { get; set; }
        public string? Anotacion { get; set; }
    }
}

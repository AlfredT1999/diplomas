﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblEstatusOrganismo
    {
        public TblEstatusOrganismo()
        {
            TblOrganismos = new HashSet<TblOrganismo>();
            TblOrganismosSolicituds = new HashSet<TblOrganismosSolicitud>();
        }

        public int Id { get; set; }
        public string? Estatus { get; set; }
        public DateTime? Inclusion { get; set; }

        public virtual ICollection<TblOrganismo> TblOrganismos { get; set; }
        public virtual ICollection<TblOrganismosSolicitud> TblOrganismosSolicituds { get; set; }
    }
}

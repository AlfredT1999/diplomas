﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatEstruorganicaClave
    {
        public CatEstruorganicaClave()
        {
            TblEstruorganicas = new HashSet<TblEstruorganica>();
        }

        public int Id { get; set; }
        public string Sigla { get; set; } = null!;
        public string Descripcion { get; set; } = null!;
        public int Nivel { get; set; }

        public virtual ICollection<TblEstruorganica> TblEstruorganicas { get; set; }
    }
}

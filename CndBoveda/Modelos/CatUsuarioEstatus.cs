﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatUsuarioEstatus
    {
        public CatUsuarioEstatus()
        {
            CatUsuarios = new HashSet<CatUsuario>();
        }

        public int UsesId { get; set; }
        public string UsesEstatus { get; set; } = null!;
        public string UsesDescripcio { get; set; } = null!;

        public virtual ICollection<CatUsuario> CatUsuarios { get; set; }
    }
}

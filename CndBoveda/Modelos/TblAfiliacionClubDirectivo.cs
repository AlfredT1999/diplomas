﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblAfiliacionClubDirectivo
    {
        public int Id { get; set; }
        public int TblAfiliacionClubId { get; set; }
        public string Nombre { get; set; } = null!;
        public int CatCargoDirectivoId { get; set; }
        public DateTime Inclusion { get; set; }

        public virtual CatCargoDirectivo CatCargoDirectivo { get; set; } = null!;
        public virtual TblAfiliacionClub TblAfiliacionClub { get; set; } = null!;
    }
}

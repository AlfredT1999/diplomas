﻿using Microsoft.AspNetCore.Mvc;

namespace ServiciosWeb.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BovedaCatalogosController : ControllerBase
    {
        readonly Negocio.BovedaCatalogos catalogos = new();

        [HttpGet]
        [Route("boveda/select/[action]")]
        public Object GetAllOrganismos()
        {
            return catalogos.GetCatalogos("organismos");
        }

        [HttpGet]
        [Route("boveda/select/[action]")]
        public Object GetAllClaves()
        {
            return catalogos.GetCatalogos("clavealfa_organismos");
        }

        [HttpGet]
        [Route("boveda/select/[action]")]
        public Object GetAllUbicacionGeografica()
        {
            return catalogos.GetCatalogos("ubicaciongeografica");
        }

        [HttpGet]
        [Route("boveda/select/[action]")]
        public Object GetAllDeportes()
        {
            return catalogos.GetCatalogos("deportes");
        }
    }
}

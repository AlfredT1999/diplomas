﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelEstadosMunicipio
    {
        public int Id { get; set; }
        public int IdEstado { get; set; }
        public int IdMunicipio { get; set; }

        public virtual CatEstado IdEstadoNavigation { get; set; } = null!;
        public virtual CatMunicipio IdMunicipioNavigation { get; set; } = null!;
    }
}

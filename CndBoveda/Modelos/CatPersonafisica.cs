﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatPersonafisica
    {
        public CatPersonafisica()
        {
            RelPersonasFisicas = new HashSet<RelPersonasFisica>();
            TblClavealfaPersonafisicas = new HashSet<TblClavealfaPersonafisica>();
            TblPersonafisicaDocumentos = new HashSet<TblPersonafisicaDocumento>();
        }

        public int IdPersona { get; set; }
        public string Paterno { get; set; } = null!;
        public string? Materno { get; set; }
        public string Nombre { get; set; } = null!;
        public string? Curp { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int? Activo { get; set; }
        public int? IdEscolaridad { get; set; }
        public int? IdEstatusEscolaridad { get; set; }
        public string? Calle { get; set; }
        public string? NumExt { get; set; }
        public string? NumInt { get; set; }
        public int? IdGenero { get; set; }
        public bool Extranjero { get; set; }
        public string? TipoIdentificacion { get; set; }
        public string? NumeroIdentificacion { get; set; }
        public int? CatUbicaciongeograficaId { get; set; }
        public int? TblOrganismosId { get; set; }
        public string? TokenFoto { get; set; }
        public string? ComentarioInactivo { get; set; }
        public string? Colonia { get; set; }
        public string? Fm3 { get; set; }
        public string? NoPasaporte { get; set; }
        public int? TblPaisId { get; set; }
        public int? TblEquipoMultidiciplinarioId { get; set; }

        public virtual CatUbicaciongeografica? CatUbicaciongeografica { get; set; }
        public virtual ICollection<RelPersonasFisica> RelPersonasFisicas { get; set; }
        public virtual ICollection<TblClavealfaPersonafisica> TblClavealfaPersonafisicas { get; set; }
        public virtual ICollection<TblPersonafisicaDocumento> TblPersonafisicaDocumentos { get; set; }
    }
}

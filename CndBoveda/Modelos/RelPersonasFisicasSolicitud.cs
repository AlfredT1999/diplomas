﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelPersonasFisicasSolicitud
    {
        public int Id { get; set; }
        public int RelDepDiscEspId { get; set; }
        public int CatPersonafisicaId { get; set; }
        public int CatTipoPersonafisicaId { get; set; }
        public string Estatus { get; set; } = null!;
        public string Justificacion { get; set; } = null!;
    }
}

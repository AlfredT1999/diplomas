﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelUsuarioEstruorganica
    {
        public int UsuarioEstruorganicaId { get; set; }
        public int CatUsuarioId { get; set; }
        public int EstruorganicaId { get; set; }

        public virtual CatUsuario CatUsuario { get; set; } = null!;
        public virtual TblEstruorganica Estruorganica { get; set; } = null!;
    }
}

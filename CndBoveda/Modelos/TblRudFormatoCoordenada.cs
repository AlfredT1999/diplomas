﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblRudFormatoCoordenada
    {
        public int TblRudFormatoId { get; set; }
        public string Tag { get; set; } = null!;
        public int X { get; set; }
        public int Y { get; set; }

        public virtual TblRudFormato TblRudFormato { get; set; } = null!;
    }
}

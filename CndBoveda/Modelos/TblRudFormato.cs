﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblRudFormato
    {
        public TblRudFormato()
        {
            TblRudOrganismos = new HashSet<TblRudOrganismo>();
        }

        public int Id { get; set; }
        public string RutaFisica { get; set; } = null!;
        public int Periodo { get; set; }
        public string Alias { get; set; } = null!;

        public virtual ICollection<TblRudOrganismo> TblRudOrganismos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblAfiliacionClub
    {
        public TblAfiliacionClub()
        {
            TblAfiliacionClubDirectivos = new HashSet<TblAfiliacionClubDirectivo>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public int CatDeporteId { get; set; }
        public int CatDisciplinaId { get; set; }
        public int? TblOrganismosId { get; set; }
        public string? Organismo { get; set; }
        public string? Direccion { get; set; }
        public DateTime? Fecha { get; set; }
        public string? Hora { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public DateTime Inclusion { get; set; }
        public bool? Aprobado { get; set; }

        public virtual CatDeporte CatDeporte { get; set; } = null!;
        public virtual CatDisciplina CatDisciplina { get; set; } = null!;
        public virtual TblOrganismo? TblOrganismos { get; set; }
        public virtual ICollection<TblAfiliacionClubDirectivo> TblAfiliacionClubDirectivos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelDocumentosOrganismo
    {
        public int Id { get; set; }
        public int? IdDocumentoOrganismo { get; set; }
        public int? IdTipoOrganismo { get; set; }
    }
}

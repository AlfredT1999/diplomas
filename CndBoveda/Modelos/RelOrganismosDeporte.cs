﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelOrganismosDeporte
    {
        public int Id { get; set; }
        public int? TblOrganismosId { get; set; }
        public int? CatDeporteId { get; set; }
        public DateTime? Inclusion { get; set; }

        public virtual CatDeporte? CatDeporte { get; set; }
        public virtual TblOrganismo? TblOrganismos { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class ViewCatUsuario
    {
        public int UsId { get; set; }
        public int UsExpediente { get; set; }
        public string? UsPassword { get; set; }
        public string UsNombres { get; set; } = null!;
        public string UsPaterno { get; set; } = null!;
        public string UsMaterno { get; set; } = null!;
        public string UsCuenta { get; set; } = null!;
        public string UsCorreo { get; set; } = null!;
        public string UsPuesto { get; set; } = null!;
        public int? TcrId { get; set; }
        public string TcrTipo { get; set; } = null!;
        public int? UsesId { get; set; }
        public string Estatus { get; set; } = null!;
        public DateTime? Inclusion { get; set; }
        public DateTime? EnvioToken { get; set; }
        public int? UsIntentos { get; set; }
        public string NombreCompleto { get; set; } = null!;
        public int IdArea { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatPaise
    {
        public int Id { get; set; }
        public string? Iso { get; set; }
        public string? Nombre { get; set; }
    }
}

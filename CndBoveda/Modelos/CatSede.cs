﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatSede
    {
        public CatSede()
        {
            CatTipoInstalacionSedes = new HashSet<CatTipoInstalacionSede>();
            RelSedeInstalaciones = new HashSet<RelSedeInstalacione>();
        }

        public int IdSede { get; set; }
        public string Nombre { get; set; } = null!;
        public bool Activo { get; set; }

        public virtual ICollection<CatTipoInstalacionSede> CatTipoInstalacionSedes { get; set; }
        public virtual ICollection<RelSedeInstalacione> RelSedeInstalaciones { get; set; }
    }
}

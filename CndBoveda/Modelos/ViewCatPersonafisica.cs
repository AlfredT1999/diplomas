﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class ViewCatPersonafisica
    {
        public int IdPersona { get; set; }
        public string Paterno { get; set; } = null!;
        public string? Materno { get; set; }
        public string Nombre { get; set; } = null!;
        public string? Curp { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaModificacion { get; set; }
        public int? IdEscolaridad { get; set; }
        public string IdEscolaridadAlfa { get; set; } = null!;
        public int? IdEstatusEscolaridad { get; set; }
        public string IdEstatusEscolaridadAlfa { get; set; } = null!;
        public string? Calle { get; set; }
        public string? NumExt { get; set; }
        public string? NumInt { get; set; }
        public int? IdGenero { get; set; }
        public string IdGeneroAlfa { get; set; } = null!;
        public bool Extranjero { get; set; }
        public string? TipoIdentificacion { get; set; }
        public string? NumeroIdentificacion { get; set; }
        public int? CatUbicaciongeograficaId { get; set; }
        public string? CodigoPostal { get; set; }
        public string? Colonia { get; set; }
        public string? Municipio { get; set; }
        public string? Estado { get; set; }
        public string? Ciudad { get; set; }
        public int? Activo { get; set; }
    }
}

﻿using CndDiplomas;
using CndDiplomas.ComplexModelos;
using CndDiplomas.Modelos;
using Execute;

namespace Negocio
{
    public class DiplomaNegocio
    {
        public static Object GetAllPreregistros()
        {
            try
            {
                Object Response_ = new();
                CndDiplomasContext _ctx = new();

                Response_ = _ctx.TblPreregistros.ToList();

                return Response_;
            }
            catch (Exception ex)
            {
                return Error.Set(MsgError.Select, ex.Message);
            }
        }

        public static Respuesta Preregistro(PreRegistroDTO data)
        {
            try
            {
                Respuesta respuesta = new();
                Object Response_ = GenerateArgs.ExecuteSp("sp_insert_tbl_preregistro", data);

                respuesta = (Respuesta)Response_;

                if (respuesta.Exito)
                {
                    return Success.Set(MsgSuccess.Add, (int)(respuesta.Id != null ? respuesta.Id : 0));
                }
                else
                {
                    throw new Exception("Error a la hora de agregar preregistro");
                }
            }
            catch (Exception ex)
            {
                return Error.Set(MsgError.Add, ex.Message);
            }
        }

        public static Respuesta AprobacionPreregistro(PreRegistroAprobacionDTO data)
        {
            try
            {
                Respuesta respuesta = new();
                Object Response_ = GenerateArgs.ExecuteSp("sp_insert_tbl_preregistro_aprobacion", data);

                respuesta = (Respuesta)Response_;

                if (respuesta.Exito)
                {
                    return Success.Set(MsgSuccess.Add, (int)(respuesta.Id != null ? respuesta.Id : 0));
                }
                else
                {
                    throw new Exception("Error a la hora de agregar aprobacion");
                }
            }
            catch (Exception ex)
            {
                return Error.Set(MsgError.Add, ex.Message);
            }
        }
    }
}

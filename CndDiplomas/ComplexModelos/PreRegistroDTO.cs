﻿#nullable disable

using System.ComponentModel.DataAnnotations;

namespace CndDiplomas.ComplexModelos
{
    public class PreRegistroDTO
    {
        [Required]
        public int BovedaTblOrganismoPerfisicaId { get; set; }
        public char Tipopersona { get; set; }
        [Required]
        public string Institucion { get; set; }
        [Required]
        public string Usuario { get; set; }

        [Required(ErrorMessage = "Contraseña requerida")]
        //[StringLength(255, ErrorMessage = "La contraseña debe ser de al menos 8 caracteres.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public int BovedaCatDeporteId { get; set; }
        [Required]
        public string ObjetivoInstitucio { get; set; }
        [Required]
        public string Cp { get; set; }
        [Required]
        public string Estado { get; set; }
        [Required]
        public string Municipio { get; set; }
        [Required]
        public string NumExterior { get; set; }
        public string NumInterior { get; set; }
        [Required]
        public string Telefono { get; set; }

        [Required]
        [EmailAddress]
        public string Correo { get; set; }
    }
}

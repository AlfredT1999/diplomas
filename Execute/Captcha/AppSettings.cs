﻿#nullable disable

namespace Execute.Captcha
{
    public class AppSettings
    {
        public string RecaptchaSecretKey { get; set; }
    }
}
    
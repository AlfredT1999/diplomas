﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelUsuarioOrganismo
    {
        public int Id { get; set; }
        public int CatUsuarioId { get; set; }
        public int TblOrganismoId { get; set; }
        public DateTime? Inclusion { get; set; }
    }
}

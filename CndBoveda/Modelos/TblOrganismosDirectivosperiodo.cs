﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblOrganismosDirectivosperiodo
    {
        public TblOrganismosDirectivosperiodo()
        {
            TblOrganismosDirectivos = new HashSet<TblOrganismosDirectivo>();
        }

        public int Id { get; set; }
        public int? TblOrganismosId { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public DateTime? Inclusion { get; set; }

        public virtual TblOrganismo? TblOrganismos { get; set; }
        public virtual ICollection<TblOrganismosDirectivo> TblOrganismosDirectivos { get; set; }
    }
}

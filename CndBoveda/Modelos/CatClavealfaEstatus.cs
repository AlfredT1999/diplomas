﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatClavealfaEstatus
    {
        public CatClavealfaEstatus()
        {
            TblClavealfaOrganismos = new HashSet<TblClavealfaOrganismo>();
            TblClavealfaPersonafisicas = new HashSet<TblClavealfaPersonafisica>();
        }

        public int Id { get; set; }
        public string Estatus { get; set; } = null!;
        public string Descripcion { get; set; } = null!;
        public DateTime Inclusion { get; set; }

        public virtual ICollection<TblClavealfaOrganismo> TblClavealfaOrganismos { get; set; }
        public virtual ICollection<TblClavealfaPersonafisica> TblClavealfaPersonafisicas { get; set; }
    }
}

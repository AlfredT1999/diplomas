﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RudsOrganismosRenadeTest
    {
        public int Id { get; set; }
        public int? SolIdRenade { get; set; }
        public string RfcRenade { get; set; } = null!;
        public string RazonSocialRenade { get; set; } = null!;
        public int? RudIdRenade { get; set; }
        public string? RudRenade { get; set; }
        public DateTime? SolExpiryDateRenade { get; set; }
    }
}

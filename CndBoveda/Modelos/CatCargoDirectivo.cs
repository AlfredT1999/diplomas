﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatCargoDirectivo
    {
        public CatCargoDirectivo()
        {
            TblAfiliacionClubDirectivos = new HashSet<TblAfiliacionClubDirectivo>();
            TblOrganismosDirectivos = new HashSet<TblOrganismosDirectivo>();
        }

        public int Id { get; set; }
        public string Cargo { get; set; } = null!;
        public DateTime Inclusion { get; set; }
        public bool Activo { get; set; }

        public virtual ICollection<TblAfiliacionClubDirectivo> TblAfiliacionClubDirectivos { get; set; }
        public virtual ICollection<TblOrganismosDirectivo> TblOrganismosDirectivos { get; set; }
    }
}

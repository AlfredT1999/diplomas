﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblCorreo
    {
        public int Id { get; set; }
        public string Correo { get; set; } = null!;
        public string? Organismo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblPersonafisicaDocumento
    {
        public int Id { get; set; }
        public int? CatPersonafisicaId { get; set; }
        public string? TokenDocumento { get; set; }
        public string? NombreDocumento { get; set; }
        public DateTime? Inclusion { get; set; }

        public virtual CatPersonafisica? CatPersonafisica { get; set; }
    }
}

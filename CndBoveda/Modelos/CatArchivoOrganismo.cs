﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatArchivoOrganismo
    {
        public CatArchivoOrganismo()
        {
            ArchivosOrganismos = new HashSet<ArchivosOrganismo>();
        }

        public int Id { get; set; }
        public string NombreDocumento { get; set; } = null!;
        public bool DocumentoBase { get; set; }
        public int CatTipoOrganismoId { get; set; }
        public bool? Activo { get; set; }

        public virtual ICollection<ArchivosOrganismo> ArchivosOrganismos { get; set; }
    }
}

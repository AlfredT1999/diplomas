﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelSedeInstalacione
    {
        public int IdSedeInstalacion { get; set; }
        public int IdSede { get; set; }
        public int IdInstalacion { get; set; }
        public bool? Activo { get; set; }

        public virtual CatInstalacione IdInstalacionNavigation { get; set; } = null!;
        public virtual CatSede IdSedeNavigation { get; set; } = null!;
    }
}

﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblOrganismo
    {
        public TblOrganismo()
        {
            RelOrganismosDeportes = new HashSet<RelOrganismosDeporte>();
            TblAfiliacionClubs = new HashSet<TblAfiliacionClub>();
            TblClavealfaOrganismos = new HashSet<TblClavealfaOrganismo>();
            TblFisicaSolicituds = new HashSet<TblFisicaSolicitud>();
            TblOrganismosDirectivosperiodos = new HashSet<TblOrganismosDirectivosperiodo>();
            TblOrganismosDocumentos = new HashSet<TblOrganismosDocumento>();
            TblRudOrganismos = new HashSet<TblRudOrganismo>();
        }

        public int Id { get; set; }
        public string? Descripcion { get; set; }
        public int? CatTipoOrganismosId { get; set; }
        public int? TblEstatusOrganismoId { get; set; }
        public int? CatDeporteId { get; set; }
        public string? Rfc { get; set; }
        public int? CatUbicaciongeograficaId { get; set; }
        public string Calle { get; set; } = null!;
        public string NumExt { get; set; } = null!;
        public string? NumInt { get; set; }
        public string Cp { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string? TokenLogo { get; set; }
        public bool? Activo { get; set; }
        public string? ComentarioInactivo { get; set; }
        public DateTime? Inclusion { get; set; }
        public string Titulo { get; set; } = null!;
        public string? Cargo { get; set; }
        public string? Colonia { get; set; }
        public int? RelEstadosMunicipiosId { get; set; }
        public string? Telefonos { get; set; }
        public string? Contacto { get; set; }
        public string? Foto { get; set; }
        public string? TitularNombres { get; set; }
        public string? TitularApePat { get; set; }
        public string? TitularApeMat { get; set; }
        public DateTime? FechaNac { get; set; }
        public string? Talla { get; set; }
        public string? ContactoTelefono { get; set; }

        public virtual CatDeporte? CatDeporte { get; set; }
        public virtual CatTipoOrganismo? CatTipoOrganismos { get; set; }
        public virtual CatUbicaciongeografica? CatUbicaciongeografica { get; set; }
        public virtual TblEstatusOrganismo? TblEstatusOrganismo { get; set; }
        public virtual ICollection<RelOrganismosDeporte> RelOrganismosDeportes { get; set; }
        public virtual ICollection<TblAfiliacionClub> TblAfiliacionClubs { get; set; }
        public virtual ICollection<TblClavealfaOrganismo> TblClavealfaOrganismos { get; set; }
        public virtual ICollection<TblFisicaSolicitud> TblFisicaSolicituds { get; set; }
        public virtual ICollection<TblOrganismosDirectivosperiodo> TblOrganismosDirectivosperiodos { get; set; }
        public virtual ICollection<TblOrganismosDocumento> TblOrganismosDocumentos { get; set; }
        public virtual ICollection<TblRudOrganismo> TblRudOrganismos { get; set; }
    }
}

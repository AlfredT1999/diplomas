﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class Tblog
    {
        public int Id { get; set; }
        public string Error { get; set; } = null!;
        public string? Descripcion { get; set; }
        public string? DescripcionCorta { get; set; }
        public string Sp { get; set; } = null!;
        public string Segmento { get; set; } = null!;
        public int? Line { get; set; }
        public int? CatSistemaId { get; set; }
        public DateTime? Inclusion { get; set; }
        public int? IdNumerico { get; set; }
        public string? IdAlfa { get; set; }

        public virtual CatSistema? CatSistema { get; set; }
    }
}

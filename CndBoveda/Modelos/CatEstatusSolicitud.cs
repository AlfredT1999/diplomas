﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatEstatusSolicitud
    {
        public CatEstatusSolicitud()
        {
            TblFisicaSolicituds = new HashSet<TblFisicaSolicitud>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
        public bool Activo { get; set; }
        public DateTime Inclusion { get; set; }

        public virtual ICollection<TblFisicaSolicitud> TblFisicaSolicituds { get; set; }
    }
}

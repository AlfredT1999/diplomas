﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatTipoContacto
    {
        public int Id { get; set; }
        public string Tipo { get; set; } = null!;
        public string TipoContacto { get; set; } = null!;
    }
}

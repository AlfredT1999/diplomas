﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblFederacione
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdFederacion { get; set; }
    }
}

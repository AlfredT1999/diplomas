﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class TblMovUsuario
    {
        public TblMovUsuario()
        {
            TblLogMovUsuarios = new HashSet<TblLogMovUsuario>();
        }

        public int TmuId { get; set; }
        public int UsId { get; set; }
        public int UsExpediente { get; set; }
        public string? UsPassword { get; set; }
        public string UsNombres { get; set; } = null!;
        public string UsPaterno { get; set; } = null!;
        public string UsMaterno { get; set; } = null!;
        public string UsCuenta { get; set; } = null!;
        public string UsCorreo { get; set; } = null!;
        public string UsPuesto { get; set; } = null!;
        public int? TcrId { get; set; }
        public int? UsesId { get; set; }
        public DateTime? UsInclusion { get; set; }
        public DateTime? TmuFechaModificacion { get; set; }
        public int TmuUsIdResponsable { get; set; }

        public virtual CatUsuario TmuUsIdResponsableNavigation { get; set; } = null!;
        public virtual ICollection<TblLogMovUsuario> TblLogMovUsuarios { get; set; }
    }
}

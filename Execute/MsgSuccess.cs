﻿using CndDiplomas;

namespace Execute
{
    public class MsgSuccess
    {
        public const String Add = "Se agregó elemento";
        public const String Update = "Se actualizó elemento";
        public const String Status = "Se actualizó estatus";
        public const String Select = "Se encontrarón elementos para listar";
        public const String Empty = "No se encontrarón resultados";
        public const String EmptyDelegacion = "No se encontró delegación activa";
        public const String Remove = "Se eliminó elemento";
        public const String RemoveRange = "Se eliminó la lista de elementos";
        public const String Validation = "Validación Exitosa";
        public const String SelectValidacionInstalaciones = "Se encontraron los siguientes servicios disponibles";
        public const String EmptyValidacionInstalaciones = "No se encontraron servicios disponibles";
        public const String EmptyValidacionAlimentos = "No se encontraron alimentos disponibles";
        public const String EmptyTrazaDelegacion = "No se encontraron registros de acceso";
    }
    public static class Success
    {
        public static Respuesta Set(String tipe)
        {
            Respuesta Respuesta_ = new()
            {
                Exito = true,
                Mensaje = tipe
            };

            return Respuesta_;
        }


        public static Respuesta Set(String tipe, String Anotacion)
        {
            Respuesta Respuesta_ = new()
            {
                Exito = true,
                Mensaje = tipe,
                Anotacion = Anotacion
            };

            return Respuesta_;
        }

        public static Respuesta Set(String tipe, Object obj)
        {
            Respuesta Respuesta_ = new()
            {
                Exito = true,
                Mensaje = tipe,
                Objeto = obj
            };

            return Respuesta_;
        }

        public static Respuesta Set(String tipe, Object obj, String anotacion)
        {
            Respuesta Respuesta_ = new()
            {
                Exito = true,
                Mensaje = tipe,
                Objeto = obj,
                Anotacion = anotacion
            };

            return Respuesta_;
        }

        public static Respuesta Set(String tipe, int id)
        {
            Respuesta Respuesta_ = new()
            {
                Id = id,
                Exito = true,
                Mensaje = tipe
            };

            return Respuesta_;
        }

        public static Respuesta Set(String tipe, Object obj, int id)
        {
            Respuesta Respuesta_ = new()
            {
                Id = id,
                Exito = true,
                Mensaje = tipe,
                Objeto = obj
            };

            return Respuesta_;
        }
        public static Respuesta Set(String tipe, Object obj, String anotacion, int id)
        {
            Respuesta Respuesta_ = new()
            {
                Id = id,
                Exito = true,
                Mensaje = tipe,
                Objeto = obj,
                Anotacion = anotacion
            };

            return Respuesta_;
        }
    }
}

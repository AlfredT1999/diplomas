﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class RelDepDiscEsp
    {
        public RelDepDiscEsp()
        {
            RelPersonasFisicas = new HashSet<RelPersonasFisica>();
        }

        public int Id { get; set; }
        public int CatDeporteId { get; set; }
        public int CatDiciplinaId { get; set; }
        public int CatEspecialidadId { get; set; }
        public bool EsOlimpico { get; set; }

        public virtual CatDeporte CatDeporte { get; set; } = null!;
        public virtual CatDisciplina CatDiciplina { get; set; } = null!;
        public virtual CatEspecialidad CatEspecialidad { get; set; } = null!;
        public virtual ICollection<RelPersonasFisica> RelPersonasFisicas { get; set; }
    }
}

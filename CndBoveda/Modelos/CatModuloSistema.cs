﻿using System;
using System.Collections.Generic;

namespace CndBoveda.Modelos
{
    public partial class CatModuloSistema
    {
        public CatModuloSistema()
        {
            TblLogSistemas = new HashSet<TblLogSistema>();
        }

        public int Id { get; set; }
        public int CatSistemaId { get; set; }
        public string Modulo { get; set; } = null!;

        public virtual CatSistema CatSistema { get; set; } = null!;
        public virtual ICollection<TblLogSistema> TblLogSistemas { get; set; }
    }
}
